﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizaPozycjiZamowienia.Model;

namespace analizaPozycjiZamowienia.Controllers
{
    public class ArtykulController
    {
        public static List<ArtykulModel> PobierzArtykuly()
        {
            List<ArtykulModel> artykuly;

            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                artykuly = (from z in entity.ARTYKUL
                            where z.ID_MAGAZYNU == ParametryUruchomienioweController.idMagazynu
                            orderby z.NAZWA ascending
                            select new ArtykulModel()
                            {
                                ID_ARTYKULU = z.ID_ARTYKULU,
                                ID_MAGAZYNU = z.ID_MAGAZYNU,
                                ID_CENY_DOM = z.ID_CENY_DOM,
                                NAZWA_CENY_DOM = z.CENA.NAZWA,
                                INDEKS_HANDLOWY = z.INDEKS_HANDLOWY,
                                INDEKS_KATALOGOWY = z.INDEKS_KATALOGOWY,
                                KOD_KRESKOWY = z.KOD_KRESKOWY,
                                NAZWA = z.NAZWA,
                                NAZWA_ORYG = z.NAZWA_ORYG,
                                STAN = z.STAN,
                                ID_JEDNOSTKI = z.ID_JEDNOSTKI,
                                JEDNOSTKA_SKROT = z.JEDNOSTKA.SKROT,
                                POLE2 = z.POLE2,
                                POLE3 = z.POLE3,
                                POLE6 = z.POLE6
                            }).ToList();
            }

            return artykuly;
        }
    }
}
