﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizaPozycjiZamowienia.Model;
using analizaPozycjiZamowienia.Controllers;

namespace analizaPozycjiZamowienia.Controllers
{
    public static class C_imag_analizy_configuracjaController
    {
        public static List<C_imag_analizy_configuracjaModel> PobierzPozycjeKonfiguracji()
        {
            List<C_imag_analizy_configuracjaModel> pozycjeKonfiguracji;

            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                pozycjeKonfiguracji = (from pk in entity.C_imag_analizy_configuracja
                                       orderby pk.id ascending
                                       select new C_imag_analizy_configuracjaModel()
                                       {
                                           id = pk.id,
                                           pozycja_konfiguracji = pk.pozycja_konfiguracji,
                                           wartosc_konfiguracji = pk.wartosc_konfiguracji
                                       }).ToList();
            }

            return pozycjeKonfiguracji;
        }

        public static void ModyfikujPozycjeKonfiguracji(C_imag_analizy_configuracjaModel parametr)
        {
            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                C_imag_analizy_configuracja pozycjeKonfiguracji = (from pk in entity.C_imag_analizy_configuracja
                                                                   where pk.id == parametr.id
                                                                   select pk).FirstOrDefault();
                pozycjeKonfiguracji.wartosc_konfiguracji = parametr.wartosc_konfiguracji;
                entity.SaveChanges();
            }
        }
    }
}
