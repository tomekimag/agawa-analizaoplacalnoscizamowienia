﻿using analizaPozycjiZamowienia.Helper;
using analizaPozycjiZamowienia.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Controllers
{
    public class CenaController
    {
        public CENA_ARTYKULU PobierzCeneArtykulu(decimal idCeny, decimal idArtykulu)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                CENA_ARTYKULU cenaArtykulu = ctx.CENA_ARTYKULU.Where(ca => ca.ID_CENY == idCeny && ca.ID_ARTYKULU == idArtykulu).SingleOrDefault();
                return cenaArtykulu;
            }
        }

        public PobierzKursWalutyResult PobierzKursWaluty(string symWalCen, string symWalArt, DateTime data, int nbpEcb = 0)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                string query = $@"DECLARE @przeliczKursWalutyRes TABLE ([KursSredni] DECIMAL(16,4),[DataKursuInt] INT)

                    INSERT @przeliczKursWalutyRes
                    EXECUTE [dbo].[AP_PobierzKursWaluty] 
                        @sym_wal_cen
                        ,@sym_wal_art
                        ,@data
                        ,@nbp_ecb

                    SELECT * FROM @przeliczKursWalutyRes";

                PobierzKursWalutyResult res = ctx.Database.SqlQuery<PobierzKursWalutyResult>(query, new List<SqlParameter>() {
                    new SqlParameter(){ ParameterName="@sym_wal_cen", Value=symWalCen, DbType=DbType.String, Size=6, Direction=ParameterDirection.Input },
                    new SqlParameter(){ ParameterName="@sym_wal_art", Value=symWalArt, DbType=DbType.String, Size=6, Direction=ParameterDirection.Input },
                    new SqlParameter(){ ParameterName="@data", Value=DateHelper.DateToInt(data) , DbType=DbType.Int32, Size=6, Direction=ParameterDirection.Input },
                    new SqlParameter(){ ParameterName="@nbp_ecb", Value=nbpEcb, DbType=DbType.Int32, Direction=ParameterDirection.Input }
                }.ToArray()).SingleOrDefault();

                return res;
            }
        }
    }
}
