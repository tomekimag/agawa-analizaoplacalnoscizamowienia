﻿using analizaPozycjiZamowienia.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Controllers
{
    public class JednostkaController
    {
        public JEDNOSTKA PobierzJednostkePoId(decimal idJednostki)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                JEDNOSTKA jednostka = ctx.JEDNOSTKA.Where(j => j.ID_JEDNOSTKI == idJednostki).SingleOrDefault();
                return jednostka;
            }
        }
    }
}
