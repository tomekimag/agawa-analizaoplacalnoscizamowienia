﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizaPozycjiZamowienia.Model;
using analizaPozycjiZamowienia.Controllers;

namespace analizaPozycjiZamowienia.Controllers
{
    public static class PozycjaZamowieniaController
    {
        public static List<PozycjaZamowieniaModel> PobierzPozycjeZamowieniaDoDostawcy()
        {
            List<PozycjaZamowieniaModel> pozycjeZamowienia;

            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                pozycjeZamowienia = (from pz in entity.POZYCJA_ZAMOWIENIA
                                     join a in entity.ARTYKUL on pz.ID_ARTYKULU equals a.ID_ARTYKULU
                                     where pz.ID_ZAMOWIENIA == ParametryUruchomienioweController.idZamowienia
                                     select new PozycjaZamowieniaModel()
                                     {
                                         NAZWA = a.NAZWA,
                                         POLE6Artykulu = a.POLE6,
                                         INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY,
                                         INDEKS_HANDLOWY = a.INDEKS_HANDLOWY,

                                         ID_POZYCJI_ZAMOWIENIA = pz.ID_POZYCJI_ZAMOWIENIA,
                                         ID_ZAMOWIENIA = pz.ID_ZAMOWIENIA,
                                         ID_ARTYKULU = pz.ID_ARTYKULU,
                                         ID_WARIANTU = pz.ID_WARIANTU,
                                         KOD_VAT = pz.KOD_VAT,
                                         ZAMOWIONO = pz.ZAMOWIONO,
                                         ZREALIZOWANO = pz.ZREALIZOWANO,
                                         DO_REALIZACJI = pz.DO_REALIZACJI,
                                         ZAREZERWOWANO = pz.ZAREZERWOWANO,
                                         DO_REZERWACJI = pz.DO_REZERWACJI,
                                         CENA_NETTO = pz.CENA_NETTO,
                                         CENA_BRUTTO = pz.CENA_BRUTTO,
                                         CENA_NETTO_WAL = pz.CENA_NETTO_WAL,
                                         CENA_BRUTTO_WAL = pz.CENA_BRUTTO_WAL,
                                         PRZELICZNIK = pz.PRZELICZNIK,
                                         JEDNOSTKA = pz.JEDNOSTKA,
                                         NARZUT = pz.NARZUT,
                                         DO_REZ_USER = pz.DO_REZ_USER,
                                         DO_REZ_POP = pz.DO_REZ_POP,
                                         STAN_ZREALIZOWANO = pz.STAN_ZREALIZOWANO,
                                         TYP = pz.TYP,
                                         OPIS = pz.OPIS,
                                         TRYBREJESTRACJI = pz.TRYBREJESTRACJI,
                                         ZNACZNIK_CENY = pz.ZNACZNIK_CENY,
                                         ID_POZYCJI_OFERTY = pz.ID_POZYCJI_OFERTY,
                                         ID_DOSTAWY_REZ = pz.ID_DOSTAWY_REZ,
                                         POLE1 = pz.POLE1,
                                         POLE2 = pz.POLE2,
                                         POLE3 = pz.POLE3,
                                         POLE4 = pz.POLE4,
                                         POLE5 = pz.POLE5,
                                         POLE6 = pz.POLE6,
                                         POLE7 = pz.POLE7,
                                         POLE8 = pz.POLE8,
                                         POLE9 = pz.POLE9,
                                         POLE10 = pz.POLE10,
                                         NR_SERII = pz.NR_SERII
                                     }).ToList();
            }

            return pozycjeZamowienia;
        }
    }
}
