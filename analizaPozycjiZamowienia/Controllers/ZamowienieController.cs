﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Model;

namespace analizaPozycjiZamowienia.Controllers
{
    public static class ZamowienieController
    {
        public static ZamowienieModel PobierzZamowienie()
        {
            ZamowienieModel zamowienie;

            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                zamowienie = (from z in entity.ZAMOWIENIE
                              where z.ID_ZAMOWIENIA == ParametryUruchomienioweController.idZamowienia
                              select new ZamowienieModel()
                              {
                                   ID_ZAMOWIENIA = z.ID_ZAMOWIENIA,
                                  ID_KONTRAHENTA = z.ID_KONTRAHENTA,
                                  ID_FIRMY  = z.ID_FIRMY,
                                  ID_MAGAZYNU = z.ID_MAGAZYNU,
                                  NUMER = z.NUMER,
                                  DATA = z.DATA,
                                  DATA_REALIZACJI = z.DATA_REALIZACJI,
                                  ZALICZKA = z.ZALICZKA,
                                  PRIORYTET = z.PRIORYTET,
                                  AUTO_REZERWACJA = z.AUTO_REZERWACJA,
                                  NR_ZAMOWIENIA_KLIENTA = z.NR_ZAMOWIENIA_KLIENTA,
                                  TYP = z.TYP,
                                  AUTONUMER = z.AUTONUMER,
                                  WARTOSC_BRUTTO = z.WARTOSC_BRUTTO,
                                  WARTOSC_NETTO = z.WARTOSC_NETTO,
                                  WARTOSC_NETTO_WAL = z.WARTOSC_NETTO_WAL,
                                  WARTOSC_BRUTTO_WAL = z.WARTOSC_BRUTTO_WAL,
                                  PRZELICZNIK_WAL = z.PRZELICZNIK_WAL,
                                  SYM_WAL = z.SYM_WAL,
                                  DOK_WAL = z.DOK_WAL,
                                  DATA_KURS_WAL = z.DATA_KURS_WAL,
                                  SEMAFOR = z.SEMAFOR,
                                  STAN_REALIZ = z.STAN_REALIZ,
                                  STATUS_ZAM = z.STATUS_ZAM,
                                  FLAGA_STANU = z.FLAGA_STANU,
                                  UWAGI = z.UWAGI,
                                  ID_PRACOWNIKA = z.ID_PRACOWNIKA,
                                  ID_UZYTKOWNIKA = z.ID_UZYTKOWNIKA,
                                  POLE1 = z.POLE1,
                                  POLE2 = z.POLE2,
                                  POLE3 = z.POLE3,
                                  POLE4 = z.POLE4,
                                  POLE5 = z.POLE5,
                                  POLE6 = z.POLE6,
                                  POLE7 = z.POLE7,
                                  POLE8 = z.POLE8,
                                  POLE9 = z.POLE9,
                                  POLE10 = z.POLE10,
                                  DOK_ZABLOKOWANY = z.DOK_ZABLOKOWANY,
                                  TRYBREJESTRACJI = z.TRYBREJESTRACJI,
                                  RABAT_NARZUT = z.RABAT_NARZUT,
                                  OBLICZANIE_WG_CEN = z.OBLICZANIE_WG_CEN,
                                  ID_RACHUNKU = z.ID_RACHUNKU,
                                  ID_ETYKIETY = z.ID_ETYKIETY,
                                  INFORMACJE_DODATKOWE = z.INFORMACJE_DODATKOWE,
                                  ZAMOWIENIE_INTERNETOWE = z.ZAMOWIENIE_INTERNETOWE,
                                  KOD_KRESKOWY = z.KOD_KRESKOWY,
                                  KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA,
                                  ID_KONTAKTU = z.ID_KONTAKTU,
                                  OSOBA_ZAMAWIAJACA = z.OSOBA_ZAMAWIAJACA,
                                  FORMA_PLATNOSCI = z.FORMA_PLATNOSCI,
                                  DNI_PLATNOSCI = z.DNI_PLATNOSCI,
                                  FAKTURA_DO_ZAMOWIENIA = z.FAKTURA_DO_ZAMOWIENIA,
                                  NUMER_PRZESYLKI = z.NUMER_PRZESYLKI,
                                  ID_OPERATORA_PRZESYLKI = z.ID_OPERATORA_PRZESYLKI,
                                  OPLACONE_INTERNETOWO = z.OPLACONE_INTERNETOWO,
                                  TYP_SYS_INTERNETOWEGO = z.TYP_SYS_INTERNETOWEGO,
                                  DATA_UTWORZENIA_WIERSZA = z.DATA_UTWORZENIA_WIERSZA,
                                  DATA_BLOKADY_WIERSZA = z.DATA_BLOKADY_WIERSZA
                              }).FirstOrDefault();
            }

            return zamowienie;
        }

        public static TYP_DOKUMENTU_MAGAZYNOWEGO PobierzTypZamowienia(string numerZamowienia, decimal idFirmy)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                TYP_DOKUMENTU_MAGAZYNOWEGO typZamowienia = ctx.TYP_DOKUMENTU_MAGAZYNOWEGO
                    .Where(tdm => tdm.RODZAJ.ToLower() == ("Zamówienie").ToLower() && tdm.ID_FIRMY == idFirmy && numerZamowienia.ToLower().StartsWith(tdm.SYGNATURA.ToLower()))
                    .SingleOrDefault();

                return typZamowienia;
            }
        }

        public static decimal DodajPozyceZamowienia(decimal idZamowienia, decimal idArtykulu, string kodVat, decimal zamowiono, decimal zrealizowano, decimal zarezerwowano, decimal doRezerwacji, decimal cenaNetto, decimal cenaBrutto, decimal cenaNettoWal, decimal cenaBruttoWal, decimal przelicznik, string jednostka, decimal narzut, string opis, byte znakNarzutu, byte trybRejestracji, decimal? idDostawyRez, decimal? idWariantuProduktu, string znacznikCeny, string nrSerii)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                ObjectParameter idPozycjiZamowieniaParam = new ObjectParameter("ID_POZYCJI_ZAMOWIENIA", typeof(decimal));
                int dodajPozycjeZamowieniaRes = ctx.RM_DodajPozycjeZamowienia_Server(idPozycjiZamowieniaParam, idZamowienia, idArtykulu, kodVat, zamowiono, zrealizowano, zarezerwowano, doRezerwacji, cenaNetto, cenaBrutto, cenaNettoWal, cenaBruttoWal, przelicznik, jednostka, narzut, opis, znakNarzutu, trybRejestracji, idDostawyRez, idWariantuProduktu, znacznikCeny, nrSerii);

                //if (dodajPozycjeZamowieniaRes == 0)
                //{
                    decimal idDodanejPozycjiZamowienia = Convert.ToDecimal(idPozycjiZamowieniaParam.Value);
                    return idDodanejPozycjiZamowienia;
                //}
                //else
                    //throw new Exception($"Wystąpił błąd podczas dodawania pozycji zamowienia (IdZamowienia: {idZamowienia})");
            }
        }

        public static bool PoprawPozyceZamowienia(decimal idPozycjiZamowienia, decimal idZamowienia, decimal idArtykulu, string kodVat, decimal zamowiono, decimal zrealizowano, decimal zarezerwowano, decimal doRezerwacji, decimal cenaNetto, decimal cenaBrutto, decimal cenaNettoWal, decimal cenaBruttoWal, decimal przelicznik, string jednostka, decimal narzut, string opis, byte znakNarzutu, decimal? idWariantuProduktu, string znacznikCeny, string nrSerii)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                int poprawPozycjeZamowieniaRes = ctx.RM_PoprawPozycjeZamowienia(idPozycjiZamowienia, idZamowienia, idArtykulu, kodVat, zamowiono, zrealizowano, zarezerwowano, doRezerwacji, cenaNetto, cenaBrutto, cenaNettoWal, cenaBruttoWal, przelicznik, jednostka, narzut, opis, znakNarzutu, idWariantuProduktu, znacznikCeny, nrSerii);

                bool czyPozycjaZamowieniaPoprawiona;
                //if (poprawPozycjeZamowieniaRes == 0)
                //{
                czyPozycjaZamowieniaPoprawiona = true;
                //}
                //else
                //throw new Exception($"Wystąpił błąd podczas poprawiania pozycji zamowienia (IdZamowienia: {idZamowienia}, IdPozycjiZamowienia: {idPozycjiZamowienia})");

                return czyPozycjaZamowieniaPoprawiona;
            }
        }

        public static bool PoprawPozycjeZamowienia(PozycjaZamowieniaModel pozycjaZamowienia, CenaController cenaCtrl)
        {
            ZamowienieModel zamowienie = ZamowienieController.PobierzZamowienie();
            TYP_DOKUMENTU_MAGAZYNOWEGO typZamowieniwa = ZamowienieController.PobierzTypZamowienia(zamowienie.NUMER, zamowienie.ID_FIRMY);
            PobierzKursWalutyResult kursWaluty = cenaCtrl.PobierzKursWaluty("", zamowienie.SYM_WAL, DateTime.Now);

            pozycjaZamowienia.CENA_NETTO = ((pozycjaZamowienia.CENA_NETTO_WAL ?? 0) / (kursWaluty.KursSredni ?? 1));
            pozycjaZamowienia.CENA_BRUTTO = ((pozycjaZamowienia.CENA_BRUTTO_WAL ?? 0) / (kursWaluty.KursSredni ?? 1));

            bool czyPozycjaZamowieniaPoprawiona = ZamowienieController.PoprawPozyceZamowienia(
                idPozycjiZamowienia: pozycjaZamowienia.ID_POZYCJI_ZAMOWIENIA,
                idZamowienia: pozycjaZamowienia.ID_ZAMOWIENIA.Value,
                idArtykulu: pozycjaZamowienia.ID_ARTYKULU.Value,
                kodVat: pozycjaZamowienia.KOD_VAT,
                zamowiono: pozycjaZamowienia.ZAMOWIONO,
                zrealizowano: pozycjaZamowienia.ZREALIZOWANO,
                zarezerwowano: pozycjaZamowienia.ZAREZERWOWANO,
                doRezerwacji: pozycjaZamowienia.DO_REZERWACJI,
                cenaNetto: pozycjaZamowienia.CENA_NETTO,
                cenaBrutto: pozycjaZamowienia.CENA_BRUTTO,
                cenaNettoWal: pozycjaZamowienia.CENA_NETTO_WAL.Value,
                cenaBruttoWal: pozycjaZamowienia.CENA_BRUTTO_WAL.Value,
                przelicznik: pozycjaZamowienia.PRZELICZNIK,
                jednostka: pozycjaZamowienia.JEDNOSTKA,
                narzut: pozycjaZamowienia.NARZUT,
                opis: pozycjaZamowienia.OPIS,
                znakNarzutu: (byte)((pozycjaZamowienia.NARZUT >= 0) ? 2 : 0),
                idWariantuProduktu: pozycjaZamowienia.ID_WARIANTU,
                znacznikCeny: pozycjaZamowienia.ZNACZNIK_CENY,
                nrSerii: pozycjaZamowienia.NR_SERII
            );

            PobierzFormatNumeracjiResult pobierzFormatNumeracjiRes = ZamowienieController.PobierzFormatNumeracji(
                idFirmy: zamowienie.ID_FIRMY,
                dokument: 2,
                idTypu: zamowienie.TYP.Value,
                idZasobu: zamowienie.ID_MAGAZYNU
            );

            SumujZamowienieResult sumujZamowienieRes = ZamowienieController.SumujZamowienie(zamowienie.ID_ZAMOWIENIA);

            bool czyZamowienieZatwierdzony = ZamowienieController.ZatwierdzZamowienie(
                idZamowienia: zamowienie.ID_ZAMOWIENIA,
                idKontrahenta: zamowienie.ID_KONTRAHENTA.Value,
                idTypu: typZamowieniwa.ID_TYPU,
                numer: zamowienie.NUMER,
                numFormat: pobierzFormatNumeracjiRes.FormatNum,
                numOkresNumeracji: pobierzFormatNumeracjiRes.Okresnumeracji,
                numAuto: pobierzFormatNumeracjiRes.Parametr1,
                numNiezalezny: pobierzFormatNumeracjiRes.Parametr2,
                autonumer: zamowienie.AUTONUMER,
                idFirmy: zamowienie.ID_FIRMY,
                idMagazynu: zamowienie.ID_MAGAZYNU,
                data: zamowienie.DATA.Value,
                dataRealizacji: zamowienie.DATA_REALIZACJI.Value,
                zaliczka: zamowienie.ZALICZKA.Value,
                priorytet: zamowienie.PRIORYTET.Value,
                autoRezerwacja: zamowienie.AUTO_REZERWACJA.Value,
                nrZamKlienta: zamowienie.NR_ZAMOWIENIA_KLIENTA,
                typ: zamowienie.TYP.Value,
                idPracownika: zamowienie.ID_PRACOWNIKA.Value,
                przelicznikWal: zamowienie.PRZELICZNIK_WAL.Value,
                dataKursWal: zamowienie.DATA_KURS_WAL.Value,
                symWal: zamowienie.SYM_WAL,
                dokWal: zamowienie.DOK_WAL.Value,
                flagaStanu: zamowienie.FLAGA_STANU.Value,
                trybRejestracji: zamowienie.TRYBREJESTRACJI.Value,
                rabatNarzut: zamowienie.RABAT_NARZUT.Value,
                znakRabatu: ((zamowienie.RABAT_NARZUT > 0) ? (byte)2 : (byte)0),
                uwagi: zamowienie.UWAGI,
                informacjeDodatkowe: zamowienie.INFORMACJE_DODATKOWE,
                osobaZamawiajaca: zamowienie.OSOBA_ZAMAWIAJACA,
                idKontaktu: zamowienie.ID_KONTAKTU.Value,
                formaPlatnosci: zamowienie.FORMA_PLATNOSCI,
                dniPlatnosci: zamowienie.DNI_PLATNOSCI.Value,
                fakturaParagon: zamowienie.FAKTURA_DO_ZAMOWIENIA.Value,
                numerPrzesylki: zamowienie.NUMER_PRZESYLKI,
                idOperatoraPrzesylki: zamowienie.ID_OPERATORA_PRZESYLKI
            );

            return czyPozycjaZamowieniaPoprawiona;
        }

        public static bool ZatwierdzPozycjeZamowienia(decimal idPozycjiZamowienia, decimal idZamowienia, decimal idArtykulu, decimal zar0, decimal dre0, decimal dRE, decimal zam)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                int zatwierdzPozycjeZamowieniaRes = ctx.RM_ZatwierdzPozycjeZamowienia_Server(idPozycjiZamowienia, idZamowienia, idArtykulu, zar0, dre0, dRE, zam);
                bool czyPozycjaZamowieniaZatwierdzonaPoprawnie = true;// (zatwierdzPozycjeZamowieniaRes == 0);
                return czyPozycjaZamowieniaZatwierdzonaPoprawnie;
            }
        }

        public static PobierzFormatNumeracjiResult PobierzFormatNumeracji(decimal idFirmy, byte dokument, decimal idTypu, decimal idZasobu)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                ObjectParameter formatNumParam = new ObjectParameter("format_num", typeof(string));
                ObjectParameter okresNumeracjiParam = new ObjectParameter("okresnumeracji", typeof(byte));
                ObjectParameter parametr1Param = new ObjectParameter("parametr1", typeof(byte));
                ObjectParameter parametr2Param = new ObjectParameter("parametr2", typeof(byte));

                int pobierzFormatNumeracjiParam = ctx.JL_PobierzFormatNumeracji_Server((int)idFirmy, dokument, (int)idTypu, (int)idZasobu, formatNumParam, okresNumeracjiParam, parametr1Param, parametr2Param);
                //if (pobierzFormatNumeracjiParam == 0)
                //{
                    PobierzFormatNumeracjiResult pobierzFormatNumeracjiRes = new PobierzFormatNumeracjiResult()
                    {
                        FormatNum = formatNumParam.Value.ToString(),
                        Okresnumeracji = Convert.ToByte(okresNumeracjiParam.Value),
                        Parametr1 = Convert.ToByte(parametr1Param.Value),
                        Parametr2 = Convert.ToByte(parametr2Param.Value)
                    };

                    return pobierzFormatNumeracjiRes;
                //}
                //else
                    //throw new Exception($"Wystąpił błąd podczas pobrania formatu numeracji");
            }
        }

        public static SumujZamowienieResult SumujZamowienie(decimal idZamowienia)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                ObjectParameter sumaNettoParam = new ObjectParameter("suma_netto", typeof(decimal));
                ObjectParameter sumaBruttoParam = new ObjectParameter("suma_brutto", typeof(decimal));
                ObjectParameter sumaNettoWalParam = new ObjectParameter("suma_netto_wal", typeof(decimal));
                ObjectParameter sumaBruttoWalParam = new ObjectParameter("suma_brutto_wal", typeof(decimal));

                int zatwierdzPozycjeZamowieniaRes = ctx.RM_SumujZamowienie_Server(idZamowienia, sumaNettoParam, sumaBruttoParam, sumaNettoWalParam, sumaBruttoWalParam);
                //if (zatwierdzPozycjeZamowieniaRes == 0)
                //{
                    SumujZamowienieResult sumujZamowienieRes = new SumujZamowienieResult()
                    {
                        SumaNetto = Convert.ToDecimal(sumaNettoParam.Value),
                        SumaBrutto = Convert.ToDecimal(sumaBruttoParam.Value),
                        SumaNettoWal = Convert.ToDecimal(sumaNettoWalParam.Value),
                        SumaBruttoWal = Convert.ToDecimal(sumaBruttoWalParam.Value)
                    };

                    return sumujZamowienieRes;
                //}
                //else
                    ///throw new Exception($"Wystąpił błąd podczas sumowania zamówienia (IdZamowienia: {idZamowienia})");
            }
        }

        public static bool ZatwierdzZamowienie(decimal idZamowienia, decimal idKontrahenta, decimal idTypu, string numer, string numFormat, byte numOkresNumeracji, byte numAuto, byte numNiezalezny, decimal autonumer, decimal idFirmy, decimal idMagazynu, int data, int dataRealizacji, decimal zaliczka, byte priorytet, byte autoRezerwacja, string nrZamKlienta, byte typ, decimal idPracownika, decimal przelicznikWal, int dataKursWal, string symWal, byte dokWal, byte flagaStanu, byte trybRejestracji, decimal rabatNarzut, byte znakRabatu, string uwagi, string informacjeDodatkowe, string osobaZamawiajaca, decimal idKontaktu, string formaPlatnosci, int dniPlatnosci, byte fakturaParagon, string numerPrzesylki, decimal? idOperatoraPrzesylki)
        {
            using (AGAWAEntities ctx = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                ObjectParameter errMsgParam = new ObjectParameter("errMsg", typeof(string));

                int zatwierdzZamowienieRes = ctx.RM_ZatwierdzZamowienie_Server(idZamowienia, idKontrahenta, idTypu, numer, numFormat, numOkresNumeracji, numAuto, numNiezalezny, (int)autonumer, idFirmy, idMagazynu, data, dataRealizacji, zaliczka, priorytet, autoRezerwacja, nrZamKlienta, typ, idPracownika, przelicznikWal, dataKursWal, symWal, dokWal, flagaStanu, trybRejestracji, rabatNarzut, znakRabatu, uwagi, informacjeDodatkowe, osobaZamawiajaca, idKontaktu, formaPlatnosci, dniPlatnosci, fakturaParagon, numerPrzesylki, idOperatoraPrzesylki, errMsgParam);

                string errMsg = errMsgParam.Value.ToString();

                if (String.IsNullOrEmpty(errMsg))
                {
                    bool czyZammowienieZatwierdzone = true;// (zatwierdzZamowienieRes == 0);
                    return czyZammowienieZatwierdzone;
                }
                else
                    throw new Exception($"Wystąpił błąd podczas próby zatwierdzenia zamówenia ('{errMsg}')");
            }
        }
    }
}
