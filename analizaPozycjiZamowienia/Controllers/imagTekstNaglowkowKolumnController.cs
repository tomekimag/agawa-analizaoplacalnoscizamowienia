﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using analizaPozycjiZamowienia.Model;

namespace analizaPozycjiZamowienia.Controllers
{
    public class imagTekstNaglowkowKolumnController
    {
        public static List<imagTekstNaglowkowKolumnModel> PobierzTekstyNaglowkow()
        {
            List<imagTekstNaglowkowKolumnModel> tekstyNaglowkow;

            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                tekstyNaglowkow = (from z in entity.C_imag_tekst_naglowkow_kolumn
                                   orderby z.id ascending
                                   select new imagTekstNaglowkowKolumnModel()
                                   {
                                       id = z.id,
                                       nazwa_kolumny = z.nazwa_kolumny,
                                       tekst_wyswietlany = z.tekst_wyswietlany,
                                       czy_edytowalny = z.czy_edytowalny
                                   }).ToList();
            }

            return tekstyNaglowkow;
        }

        public static void ZapiszTekstNaglowka(int id, string tekstWyswietlanyNaglowka, bool czyEdytowalny)
        {
            using (AGAWAEntities entity = new AGAWAEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                var tekstNaglowka = (from z in entity.C_imag_tekst_naglowkow_kolumn
                                    where z.id == id
                                    select z).FirstOrDefault();

                if (tekstNaglowka != null)
                {
                    tekstNaglowka.tekst_wyswietlany = tekstWyswietlanyNaglowka;
                    tekstNaglowka.czy_edytowalny = czyEdytowalny;
                    entity.SaveChanges();
                }
            }
        }
    }
}
