﻿namespace analizaPozycjiZamowienia
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_edycjaPozycji = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button_naglowki = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_konfiguracja = new System.Windows.Forms.Button();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.dataGridView_analiza = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_analiza)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_edycjaPozycji);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button_naglowki);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button_konfiguracja);
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 467);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1102, 74);
            this.panel1.TabIndex = 0;
            // 
            // button_edycjaPozycji
            // 
            this.button_edycjaPozycji.Location = new System.Drawing.Point(675, 26);
            this.button_edycjaPozycji.Name = "button_edycjaPozycji";
            this.button_edycjaPozycji.Size = new System.Drawing.Size(142, 23);
            this.button_edycjaPozycji.TabIndex = 5;
            this.button_edycjaPozycji.Text = "Popraw pozycję";
            this.button_edycjaPozycji.UseVisualStyleBackColor = true;
            this.button_edycjaPozycji.Click += new System.EventHandler(this.button_edycjaPozycji_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(527, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(142, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Dodaj pozycję";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button_naglowki
            // 
            this.button_naglowki.Location = new System.Drawing.Point(352, 26);
            this.button_naglowki.Name = "button_naglowki";
            this.button_naglowki.Size = new System.Drawing.Size(142, 23);
            this.button_naglowki.TabIndex = 3;
            this.button_naglowki.Text = "Nagłówki";
            this.button_naglowki.UseVisualStyleBackColor = true;
            this.button_naglowki.Click += new System.EventHandler(this.button_naglowki_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(174, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Eksport do Excela";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_konfiguracja
            // 
            this.button_konfiguracja.Location = new System.Drawing.Point(26, 26);
            this.button_konfiguracja.Name = "button_konfiguracja";
            this.button_konfiguracja.Size = new System.Drawing.Size(142, 23);
            this.button_konfiguracja.TabIndex = 1;
            this.button_konfiguracja.Text = "Konfiguracja";
            this.button_konfiguracja.UseVisualStyleBackColor = true;
            this.button_konfiguracja.Click += new System.EventHandler(this.button_konfiguracja_Click);
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(998, 26);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 0;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // dataGridView_analiza
            // 
            this.dataGridView_analiza.AllowUserToAddRows = false;
            this.dataGridView_analiza.AllowUserToDeleteRows = false;
            this.dataGridView_analiza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_analiza.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_analiza.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_analiza.Name = "dataGridView_analiza";
            this.dataGridView_analiza.Size = new System.Drawing.Size(1102, 467);
            this.dataGridView_analiza.TabIndex = 1;
            this.dataGridView_analiza.DataSourceChanged += new System.EventHandler(this.dataGridView_analiza_DataSourceChanged);
            this.dataGridView_analiza.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_analiza_ColumnAdded);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 541);
            this.Controls.Add(this.dataGridView_analiza);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_analiza)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView_analiza;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Button button_konfiguracja;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_naglowki;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button_edycjaPozycji;
    }
}

