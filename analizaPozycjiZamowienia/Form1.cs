﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Controllers;
using analizaPozycjiZamowienia.Model;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace analizaPozycjiZamowienia
{
    public partial class Form1 : Form
    {
        private readonly CenaController _cenaCtrl;

        private List<imagTekstNaglowkowKolumnModel> tekstyNaglowkow;

        private BindingList<PozycjaZamowieniaModel> pozycje;

        private ZamowienieModel zamowienie;

        public Form1()
        {
            this._cenaCtrl = new CenaController();

            InitializeComponent();
            ZaladujZamowienie();
            ZaladujNaglowkiDlaDanychWSiatce();
            ZaladujPozycjeDoSiatki();
        }

        private void ZaladujZamowienie()
        {
            zamowienie = ZamowienieController.PobierzZamowienie();
            this.Text = $@"Numer zamówienia:{zamowienie.NUMER} Kontrahent:{zamowienie.KONTRAHENT_NAZWA}";
        }

        private void ZaladujNaglowkiDlaDanychWSiatce()
        {
            tekstyNaglowkow = imagTekstNaglowkowKolumnController.PobierzTekstyNaglowkow();
        }

        private void ZaladujPozycjeDoSiatki()
        {
            pozycje = new BindingList<PozycjaZamowieniaModel>(PozycjaZamowieniaController.PobierzPozycjeZamowieniaDoDostawcy());
            List<C_imag_analizy_configuracjaModel> pozycjeKonfiguracji = C_imag_analizy_configuracjaController.PobierzPozycjeKonfiguracji();

            decimal TRANS = pozycjeKonfiguracji.Where(pk => pk.pozycja_konfiguracji == "TRANS").Select(pk => pk.wartosc_konfiguracji).SingleOrDefault();
            decimal USD = pozycjeKonfiguracji.Where(pk => pk.pozycja_konfiguracji == "USD").Select(pk => pk.wartosc_konfiguracji).SingleOrDefault();
            decimal cena_trans_w_PLN = pozycjeKonfiguracji.Where(pk => pk.pozycja_konfiguracji == "cena trans w PLN").Select(pk => pk.wartosc_konfiguracji).SingleOrDefault();
            decimal hq40 = pozycjeKonfiguracji.Where(pk => pk.pozycja_konfiguracji == "hq40").Select(pk => pk.wartosc_konfiguracji).SingleOrDefault();

            foreach (PozycjaZamowieniaModel pozycjaZamowienia in pozycje)
            {
                pozycjaZamowienia.KOSZT_TRANS_PLN = TRANS;
                pozycjaZamowienia.USD = USD;
                pozycjaZamowienia.CENA_TRANSPORTU_PLN  = cena_trans_w_PLN;
                pozycjaZamowienia.HQ40  = hq40;

                pozycjaZamowienia.ZmienionoIloscZamowiona += this.PoprawPozycjeZamowieniaWwaproMag;
            }

            dataGridView_analiza.DataSource = pozycje;

            foreach(DataGridViewColumn column in dataGridView_analiza.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            
            dataGridView_analiza.AutoGenerateColumns = false;
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView_analiza_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            //dgv.AutoResizeColumns();
            //dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridView_analiza_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            imagTekstNaglowkowKolumnModel naglowek = tekstyNaglowkow.Where(x => x.nazwa_kolumny == e.Column.Name).FirstOrDefault();

            if (naglowek != null)
            {
                e.Column.HeaderText = naglowek.tekst_wyswietlany;
                e.Column.ReadOnly = !(naglowek.czy_edytowalny);
                e.Column.Visible = true;
            }
            else
            {
                e.Column.Visible = false;
            }

            /*
            switch (e.Column.Name)
            {
                case "INDEKS_KATALOGOWY":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "SYMBOL";
                    break;
                case "NAZWA":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "NAZWA";
                    break;
                case "ZAMOWIONO":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "PAK";
                    break;
                case "CENA_BRUTTO_WAL_PLUS_2_PROCENT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "CENA + 2%";
                    break;
                case "CENA_BRUTTO_WAL":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Cena rmb";
                    break;
                case "POLE6":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "CBM";
                    break;
                case "KOSZT_TRANS_PLN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "TRANS";
                    break;
                case "JEDNA_SETNA_METRA_SZESCIENNEGO":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "0,01m3";
                    break;
                case "USD":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "USD";
                    break;
                case "CENA_TRANSPORTU_PLN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "cena trans w PLN";
                    break;
                case "HQ40":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "hq40";
                    break;
                case "KOSZT_JEDNEGO_METRA_SZESCIENNEGO":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "koszt 1m3 PLN";
                    break;
                case "KOSZT_TRANSPORTU_1_CTN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "koszt trans 1ctn";
                    break;
                case "KOSZT_ZAKUPU_1_CTN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "koszt zakupu ctn";
                    break;
                case "KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "koszt trans Ctn + koszt zak. Ctn";
                    break;
                case "CIF_WARSZAWA":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "cif warszawa";
                    break;
                case "ZERO":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "0";
                    break;
                case "PIECDZIESIAT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "50";
                    break;
                case "SZESCDZIESIAT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "60";
                    break;
                case "SIEDEMDZIESIAT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "70";
                    break;
                case "MARZA_80_PROCENT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "marża80%";
                    break;
                case "MARZA_90_PROCENT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "marża 90%";
                    break;
                case "MARZA_100_PROCENT":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "marża 100%";
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }*/
        }

        private void button_konfiguracja_Click(object sender, EventArgs e)
        {
            Form_konfiguracja konfiguracja = new Form_konfiguracja();
            konfiguracja.ShowDialog();
            ZaladujPozycjeDoSiatki();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog zapisPliku = new SaveFileDialog();
            zapisPliku.Filter = "Excel plik|*.xlsx";
            zapisPliku.Title = "Zapis pliku excel";
            zapisPliku.ShowDialog();

            if (zapisPliku.FileName != "")
            {
                try
                {
                    using (FileStream stream = new FileStream(zapisPliku.FileName, FileMode.Create, FileAccess.Write))
                    {
                        IWorkbook wb = new XSSFWorkbook();
                        ISheet sheet = wb.CreateSheet(zamowienie.NUMER.Replace('/', '_'));
                        ICreationHelper cH = wb.GetCreationHelper();

                        IRow row = sheet.CreateRow(0);
                        ICell cell;

                        int iCell = 0;

                        foreach (DataGridViewColumn kolumna in dataGridView_analiza.Columns)
                        {
                            if (kolumna.HeaderText == "CENA_NETTO_WAL") continue;
                            cell = row.CreateCell(iCell);
                            cell.SetCellValue(cH.CreateRichTextString(kolumna.HeaderText));
                            iCell++;
                        }


                        int iRow = 1;
                        foreach (PozycjaZamowieniaModel o in pozycje)
                        {
                            row = sheet.CreateRow(iRow);

                            cell = row.CreateCell(0);
                            cell.SetCellValue(cH.CreateRichTextString(o.INDEKS_KATALOGOWY.ToString()));

                            cell = row.CreateCell(1);
                            cell.SetCellValue(cH.CreateRichTextString(o.NAZWA.ToString()));

                            cell = row.CreateCell(2);
                            cell.SetCellValue(cH.CreateRichTextString(o.ZAMOWIONO.ToString()));

                            cell = row.CreateCell(3);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$E${1 + iRow}*102/100"));
                            //p.CENA_BRUTTO_WAL_PLUS_2_PROCENT = Math.Round((p.CENA_BRUTTO_WAL ?? 0) * 102 / 100, 4);

                            cell = row.CreateCell(4);
                            cell.SetCellValue(cH.CreateRichTextString(o.CENA_BRUTTO_WAL.ToString()));

                            cell = row.CreateCell(5);
                            cell.SetCellValue(cH.CreateRichTextString(o.POLE6Artykulu.Trim().Replace('.', ',').ToString()));

                            cell = row.CreateCell(6);
                            cell.SetCellValue(cH.CreateRichTextString(o.KOSZT_TRANS_PLN.ToString()));

                            cell = row.CreateCell(7);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$G${1 + iRow}/$K${1 + iRow}/100"));
                            //p.JEDNA_SETNA_METRA_SZESCIENNEGO = Math.Round(p.KOSZT_TRANS_PLN / p.HQ40 / 100, 4);

                            cell = row.CreateCell(8);
                            cell.SetCellValue(cH.CreateRichTextString(o.USD.ToString()));

                            cell = row.CreateCell(9);
                            cell.SetCellValue(cH.CreateRichTextString(o.CENA_TRANSPORTU_PLN.ToString()));

                            cell = row.CreateCell(10);
                            cell.SetCellValue(cH.CreateRichTextString(o.HQ40.ToString()));

                            cell = row.CreateCell(11);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$J${1 + iRow}/$K${1 + iRow}/100"));
                            //p.KOSZT_JEDNEGO_METRA_SZESCIENNEGO = Math.Round(p.CENA_TRANSPORTU_PLN / p.HQ40, 4);

                            cell = row.CreateCell(12);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$F${1 + iRow}*$L${1 + iRow}/100"));
                            //p.KOSZT_TRANSPORTU_1_CTN = Math.Round(cbm * p.KOSZT_JEDNEGO_METRA_SZESCIENNEGO, 4);

                            cell = row.CreateCell(13);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$D${1 + iRow}*$C${1 + iRow}*$I${1 + iRow}"));
                            //p.KOSZT_ZAKUPU_1_CTN = Math.Round((p.CENA_BRUTTO_WAL_PLUS_2_PROCENT ?? 0) * p.ZAMOWIONO * p.USD, 4);

                            cell = row.CreateCell(14);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$M${1 + iRow}+$N${1 + iRow}"));
                            //p.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN = Math.Round(p.KOSZT_TRANSPORTU_1_CTN + p.KOSZT_ZAKUPU_1_CTN, 4);

                            cell = row.CreateCell(15);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$O${1 + iRow}/$C${1 + iRow}"));
                            //p.CIF_WARSZAWA = Math.Round(p.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN / p.ZAMOWIONO, 4);

                            cell = row.CreateCell(16);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.047*1.23"));
                            //p.ZERO = Math.Round(p.CIF_WARSZAWA * (decimal)1.047 * (decimal)1.23, 4);

                            cell = row.CreateCell(17);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.047*1.23*1.5"));
                            //p.PIECDZIESIAT = Math.Round(p.CIF_WARSZAWA * (decimal)1.047 * (decimal)1.23 * (decimal)1.5, 4);

                            cell = row.CreateCell(18);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.046*1.23*1.6"));
                            //p.SZESCDZIESIAT = Math.Round(p.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.23 * (decimal)1.6, 4);

                            cell = row.CreateCell(19);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.046*1.23*1.7"));
                            //p.SIEDEMDZIESIAT = Math.Round(p.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.23 * (decimal)1.7, 4);

                            cell = row.CreateCell(20);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.046*1.22*1.8*$I${1 + iRow}"));
                            //p.MARZA_80_PROCENT = Math.Round(p.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)1.8 * p.USD, 4);

                            cell = row.CreateCell(21);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.046*1.22*1.9*$I${1 + iRow}"));
                            //p.MARZA_90_PROCENT = Math.Round(p.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)1.9 * p.USD, 4);

                            cell = row.CreateCell(22);
                            cell.SetCellType(CellType.Formula);
                            cell.SetCellFormula(String.Format($@"$P${1 + iRow}*1.046*1.22*2.0*$I${1 + iRow}"));
                            //p.MARZA_100_PROCENT = Math.Round(p.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)2.0 * p.USD, 4);

                            iRow++;
                        }

                        wb.Write(stream);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        MessageBox.Show($@"Zapis się nie powiódł. {ex.Message}{ex.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show($@"Zapis się nie powiódł. {ex.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button_naglowki_Click(object sender, EventArgs e)
        {
            Form_naglowki naglowki = new Form_naglowki();
            if (naglowki.ShowDialog() == DialogResult.Yes)
            {
                ZaladujNaglowkiDlaDanychWSiatce();

                foreach (DataGridViewColumn kolumna in dataGridView_analiza.Columns)
                {
                    if (kolumna.Name == "CENA_NETTO_WAL") continue;

                    imagTekstNaglowkowKolumnModel naglowek = tekstyNaglowkow.Where(x => x.nazwa_kolumny == kolumna.Name).FirstOrDefault();
                    if(naglowek != null)
                    {
                        kolumna.HeaderText = naglowek.tekst_wyswietlany;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form_dodawaniePozycji nowaPozycja = new Form_dodawaniePozycji();
            if (nowaPozycja.ShowDialog() == DialogResult.Yes)
            {
                this.ZaladujPozycjeDoSiatki();
            }
        }

        private void button_edycjaPozycji_Click(object sender, EventArgs e)
        {
            //PozycjaZamowieniaModel
            string indeksKatalogowy;
            decimal zamowiono;
            Nullable<decimal> cenaNettoWal = null;

            if (dataGridView_analiza.SelectedRows == null)
            {
                MessageBox.Show("Wskarz wskaźnikiem myszy rekord który chcesz poprawić.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_analiza.SelectedRows.Count != 0)
            {
                indeksKatalogowy = dataGridView_analiza["INDEKS_KATALOGOWY", dataGridView_analiza.SelectedRows[0].Index].Value.ToString();
                zamowiono = decimal.Parse(dataGridView_analiza["ZAMOWIONO", dataGridView_analiza.SelectedRows[0].Index].Value.ToString());
                cenaNettoWal = decimal.Parse(dataGridView_analiza["CENA_NETTO_WAL", dataGridView_analiza.SelectedRows[0].Index].Value.ToString());

                PozycjaZamowieniaModel pozycja = pozycje.Where(x=> x.INDEKS_KATALOGOWY == indeksKatalogowy && x.ZAMOWIONO == zamowiono &&
                    x.CENA_NETTO_WAL == cenaNettoWal).FirstOrDefault();
                List<PozycjaZamowieniaModel> pozycjeZamowieniaOtymSamymIndeksieHandlowym = this.pozycje.Where(pz => pz.INDEKS_HANDLOWY == pozycja.INDEKS_HANDLOWY && pz.ID_POZYCJI_ZAMOWIENIA != pozycja.ID_POZYCJI_ZAMOWIENIA).ToList();

                if (pozycja != null)
                {
                    Form_edycjaPozycji edycja = new Form_edycjaPozycji(pozycja, pozycjeZamowieniaOtymSamymIndeksieHandlowym);
                    if (edycja.ShowDialog() == DialogResult.Yes)
                    {
                        //dataGridView_analiza.DataSource = null;
                        //dataGridView_analiza.DataSource = pozycje;
                        this.ZaladujPozycjeDoSiatki();
                    }
                }
            }
        }

        private void PoprawPozycjeZamowieniaWwaproMag(PozycjaZamowieniaModel pozycjaZamowienia)
        {
            bool czyPozycjaZamowieniaPoprawiona = ZamowienieController.PoprawPozycjeZamowienia(pozycjaZamowienia, this._cenaCtrl);
        }
    }
}
