﻿namespace analizaPozycjiZamowienia
{
    partial class Form_artykuly
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_wszystko = new System.Windows.Forms.Button();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.textBox_szukanyCiag = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_poleSzukania = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_wybierz = new System.Windows.Forms.Button();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.dataGridView_artykuly = new System.Windows.Forms.DataGridView();
            this.NazwaKoloruTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NrKoloruTextBox = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_artykuly)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.NrKoloruTextBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.NazwaKoloruTxtBox);
            this.panel1.Controls.Add(this.button_wszystko);
            this.panel1.Controls.Add(this.button_szukaj);
            this.panel1.Controls.Add(this.textBox_szukanyCiag);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox_poleSzukania);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(758, 87);
            this.panel1.TabIndex = 0;
            // 
            // button_wszystko
            // 
            this.button_wszystko.Location = new System.Drawing.Point(544, 7);
            this.button_wszystko.Name = "button_wszystko";
            this.button_wszystko.Size = new System.Drawing.Size(138, 23);
            this.button_wszystko.TabIndex = 4;
            this.button_wszystko.Text = "Wszystko";
            this.button_wszystko.UseVisualStyleBackColor = true;
            this.button_wszystko.Click += new System.EventHandler(this.button_wszystko_Click);
            // 
            // button_szukaj
            // 
            this.button_szukaj.Location = new System.Drawing.Point(463, 7);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(75, 23);
            this.button_szukaj.TabIndex = 3;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // textBox_szukanyCiag
            // 
            this.textBox_szukanyCiag.Location = new System.Drawing.Point(276, 9);
            this.textBox_szukanyCiag.Name = "textBox_szukanyCiag";
            this.textBox_szukanyCiag.Size = new System.Drawing.Size(181, 20);
            this.textBox_szukanyCiag.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wyszukiwanie:";
            // 
            // comboBox_poleSzukania
            // 
            this.comboBox_poleSzukania.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_poleSzukania.FormattingEnabled = true;
            this.comboBox_poleSzukania.Items.AddRange(new object[] {
            "Indeks handlowy",
            "Indeks katalogowy",
            "Nazwa"});
            this.comboBox_poleSzukania.Location = new System.Drawing.Point(106, 9);
            this.comboBox_poleSzukania.Name = "comboBox_poleSzukania";
            this.comboBox_poleSzukania.Size = new System.Drawing.Size(164, 21);
            this.comboBox_poleSzukania.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_wybierz);
            this.panel2.Controls.Add(this.button_anuluj);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 421);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(758, 64);
            this.panel2.TabIndex = 1;
            // 
            // button_wybierz
            // 
            this.button_wybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wybierz.Location = new System.Drawing.Point(580, 20);
            this.button_wybierz.Name = "button_wybierz";
            this.button_wybierz.Size = new System.Drawing.Size(75, 23);
            this.button_wybierz.TabIndex = 1;
            this.button_wybierz.Text = "Wybierz";
            this.button_wybierz.UseVisualStyleBackColor = true;
            this.button_wybierz.Click += new System.EventHandler(this.button_wybierz_Click);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(661, 20);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // dataGridView_artykuly
            // 
            this.dataGridView_artykuly.AllowUserToAddRows = false;
            this.dataGridView_artykuly.AllowUserToDeleteRows = false;
            this.dataGridView_artykuly.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_artykuly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_artykuly.Location = new System.Drawing.Point(0, 87);
            this.dataGridView_artykuly.Name = "dataGridView_artykuly";
            this.dataGridView_artykuly.ReadOnly = true;
            this.dataGridView_artykuly.Size = new System.Drawing.Size(758, 334);
            this.dataGridView_artykuly.TabIndex = 2;
            this.dataGridView_artykuly.DataSourceChanged += new System.EventHandler(this.dataGridView_artykuly_DataSourceChanged);
            this.dataGridView_artykuly.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_artykuly_ColumnAdded);
            // 
            // NazwaKoloruTxtBox
            // 
            this.NazwaKoloruTxtBox.Location = new System.Drawing.Point(276, 35);
            this.NazwaKoloruTxtBox.Name = "NazwaKoloruTxtBox";
            this.NazwaKoloruTxtBox.Size = new System.Drawing.Size(181, 20);
            this.NazwaKoloruTxtBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nazwa koloru:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(192, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Numer koloru:";
            // 
            // NrKoloruTextBox
            // 
            this.NrKoloruTextBox.Location = new System.Drawing.Point(276, 60);
            this.NrKoloruTextBox.Name = "NrKoloruTextBox";
            this.NrKoloruTextBox.Size = new System.Drawing.Size(181, 20);
            this.NrKoloruTextBox.TabIndex = 7;
            // 
            // Form_artykuly
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 485);
            this.Controls.Add(this.dataGridView_artykuly);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_artykuly";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Kartoteka artykułów";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_artykuly)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView_artykuly;
        private System.Windows.Forms.Button button_wybierz;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.Button button_wszystko;
        private System.Windows.Forms.Button button_szukaj;
        private System.Windows.Forms.TextBox textBox_szukanyCiag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_poleSzukania;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NazwaKoloruTxtBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NrKoloruTextBox;
    }
}