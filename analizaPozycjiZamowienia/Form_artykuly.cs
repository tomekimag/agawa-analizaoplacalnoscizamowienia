﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Model;
using analizaPozycjiZamowienia.Controllers;

namespace analizaPozycjiZamowienia
{
    public partial class Form_artykuly : Form
    {
        private List<ArtykulModel> artykuly;

        public ArtykulModel wybranyArtykul;

        public Form_artykuly()
        {
            InitializeComponent();
            PobierzArtykulyDoSiatki();
            /*Indeks katalogowy
            Nazwa*/
            comboBox_poleSzukania.Text = "Indeks handlowy";
        }

        private void PobierzArtykulyDoSiatki()
        {
            dataGridView_artykuly.DataSource = artykuly = ArtykulController.PobierzArtykuly();
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_wybierz_Click(object sender, EventArgs e)
        {
            if (dataGridView_artykuly.SelectedRows == null)
            {
                MessageBox.Show("Wskarz artykuł.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_artykuly.SelectedRows.Count != 0)
            {
                decimal ID_ARTYKULU = decimal.Parse(dataGridView_artykuly["ID_ARTYKULU", dataGridView_artykuly.SelectedRows[0].Index].Value.ToString());

                wybranyArtykul = artykuly.Where(x => x.ID_ARTYKULU == ID_ARTYKULU).FirstOrDefault();

                if (wybranyArtykul != null)
                {
                    this.DialogResult = DialogResult.Yes;
                }
                Close();
            }
        }

        private void dataGridView_artykuly_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "ID_ARTYKULU":
                    e.Column.Visible = false;
                    //e.Column.HeaderText = "marża 90%";
                    break;
                case "ID_MAGAZYNU":
                    e.Column.Visible = false;
                    //e.Column.HeaderText = "marża 100%";
                    break;
                case "ID_CENY_DOM":
                    e.Column.Visible = false;
                    //e.Column.HeaderText = "marża 90%";
                    break;
                case "ID_JEDNOSTKI":
                    e.Column.Visible = false;
                    //e.Column.HeaderText = "marża 90%";
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_artykuly_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            List<ArtykulModel> przefiltrowaneArtykuly = new List<ArtykulModel>();

            if (comboBox_poleSzukania.Text == "Indeks katalogowy")
            {
                przefiltrowaneArtykuly = artykuly.Where(a => a.INDEKS_KATALOGOWY.ToUpper().Contains(textBox_szukanyCiag.Text.ToUpper())).ToList();
            }
            if (comboBox_poleSzukania.Text == "Nazwa")
            {
                przefiltrowaneArtykuly = artykuly.Where(a => a.NAZWA.ToUpper().Contains(textBox_szukanyCiag.Text.ToUpper())).ToList();
            }
            if (comboBox_poleSzukania.Text == "Indeks handlowy")
                przefiltrowaneArtykuly = artykuly.Where(a => a.INDEKS_HANDLOWY.ToLower().Contains(textBox_szukanyCiag.Text.ToLower())).ToList();

            string nazwaKoloru = NazwaKoloruTxtBox.Text;
            if (!String.IsNullOrEmpty(nazwaKoloru))
                przefiltrowaneArtykuly = przefiltrowaneArtykuly.Where(a => a.POLE2.ToLower().Contains(nazwaKoloru)).ToList();

            string nrKoloru = NrKoloruTextBox.Text;
            if (!String.IsNullOrEmpty(nrKoloru))
                przefiltrowaneArtykuly = przefiltrowaneArtykuly.Where(a => a.POLE3.ToLower().Contains(nrKoloru)).ToList();

            dataGridView_artykuly.DataSource = przefiltrowaneArtykuly;
        }

        private void button_wszystko_Click(object sender, EventArgs e)
        {
            dataGridView_artykuly.DataSource = artykuly;
        }
    }
}
