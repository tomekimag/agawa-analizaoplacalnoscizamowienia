﻿namespace analizaPozycjiZamowienia
{
    partial class Form_dodawaniePozycji
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_wstawPozycje = new System.Windows.Forms.Button();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_nazwa = new System.Windows.Forms.TextBox();
            this.numericUpDown_cenaBruttoWal = new System.Windows.Forms.NumericUpDown();
            this.textBox_indeksKatalogowy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_pole6 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button_wybierzArtykul = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_cenaNettoWal = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_zamowiono = new System.Windows.Forms.NumericUpDown();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaBruttoWal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaNettoWal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_zamowiono)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wstawPozycje);
            this.panel1.Controls.Add(this.button_anuluj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 287);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(517, 77);
            this.panel1.TabIndex = 0;
            // 
            // button_wstawPozycje
            // 
            this.button_wstawPozycje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wstawPozycje.Location = new System.Drawing.Point(328, 25);
            this.button_wstawPozycje.Name = "button_wstawPozycje";
            this.button_wstawPozycje.Size = new System.Drawing.Size(75, 23);
            this.button_wstawPozycje.TabIndex = 1;
            this.button_wstawPozycje.Text = "Wstaw";
            this.button_wstawPozycje.UseVisualStyleBackColor = true;
            this.button_wstawPozycje.Click += new System.EventHandler(this.button_wstawPozycje_Click);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(409, 25);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nazwa:";
            // 
            // textBox_nazwa
            // 
            this.textBox_nazwa.Enabled = false;
            this.textBox_nazwa.Location = new System.Drawing.Point(108, 39);
            this.textBox_nazwa.Name = "textBox_nazwa";
            this.textBox_nazwa.Size = new System.Drawing.Size(295, 20);
            this.textBox_nazwa.TabIndex = 2;
            // 
            // numericUpDown_cenaBruttoWal
            // 
            this.numericUpDown_cenaBruttoWal.DecimalPlaces = 2;
            this.numericUpDown_cenaBruttoWal.Location = new System.Drawing.Point(108, 161);
            this.numericUpDown_cenaBruttoWal.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.numericUpDown_cenaBruttoWal.Name = "numericUpDown_cenaBruttoWal";
            this.numericUpDown_cenaBruttoWal.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_cenaBruttoWal.TabIndex = 3;
            // 
            // textBox_indeksKatalogowy
            // 
            this.textBox_indeksKatalogowy.Enabled = false;
            this.textBox_indeksKatalogowy.Location = new System.Drawing.Point(108, 65);
            this.textBox_indeksKatalogowy.Name = "textBox_indeksKatalogowy";
            this.textBox_indeksKatalogowy.Size = new System.Drawing.Size(157, 20);
            this.textBox_indeksKatalogowy.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Indeks kat:";
            // 
            // textBox_pole6
            // 
            this.textBox_pole6.Enabled = false;
            this.textBox_pole6.Location = new System.Drawing.Point(108, 91);
            this.textBox_pole6.Name = "textBox_pole6";
            this.textBox_pole6.Size = new System.Drawing.Size(157, 20);
            this.textBox_pole6.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "cbm (POLE6):";
            // 
            // button_wybierzArtykul
            // 
            this.button_wybierzArtykul.Location = new System.Drawing.Point(409, 37);
            this.button_wybierzArtykul.Name = "button_wybierzArtykul";
            this.button_wybierzArtykul.Size = new System.Drawing.Size(63, 23);
            this.button_wybierzArtykul.TabIndex = 8;
            this.button_wybierzArtykul.Text = "Wybierz";
            this.button_wybierzArtykul.UseVisualStyleBackColor = true;
            this.button_wybierzArtykul.Click += new System.EventHandler(this.button_wybierzArtykul_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Cena brutto wal:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Cena netto wal:";
            // 
            // numericUpDown_cenaNettoWal
            // 
            this.numericUpDown_cenaNettoWal.DecimalPlaces = 2;
            this.numericUpDown_cenaNettoWal.Location = new System.Drawing.Point(108, 187);
            this.numericUpDown_cenaNettoWal.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.numericUpDown_cenaNettoWal.Name = "numericUpDown_cenaNettoWal";
            this.numericUpDown_cenaNettoWal.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_cenaNettoWal.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Zamówiono:";
            // 
            // numericUpDown_zamowiono
            // 
            this.numericUpDown_zamowiono.DecimalPlaces = 2;
            this.numericUpDown_zamowiono.Location = new System.Drawing.Point(108, 213);
            this.numericUpDown_zamowiono.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.numericUpDown_zamowiono.Name = "numericUpDown_zamowiono";
            this.numericUpDown_zamowiono.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_zamowiono.TabIndex = 12;
            this.numericUpDown_zamowiono.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Form_dodawaniePozycji
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 364);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown_zamowiono);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDown_cenaNettoWal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_wybierzArtykul);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_pole6);
            this.Controls.Add(this.textBox_indeksKatalogowy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDown_cenaBruttoWal);
            this.Controls.Add(this.textBox_nazwa);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "Form_dodawaniePozycji";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dodawana pozycja";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaBruttoWal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaNettoWal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_zamowiono)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_wstawPozycje;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_nazwa;
        private System.Windows.Forms.NumericUpDown numericUpDown_cenaBruttoWal;
        private System.Windows.Forms.TextBox textBox_indeksKatalogowy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_pole6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_wybierzArtykul;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown_cenaNettoWal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_zamowiono;
    }
}