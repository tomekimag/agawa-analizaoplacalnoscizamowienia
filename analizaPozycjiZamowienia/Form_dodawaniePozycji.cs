﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Controllers;
using analizaPozycjiZamowienia.Model;

namespace analizaPozycjiZamowienia
{
    public partial class Form_dodawaniePozycji : Form
    {
        private readonly JednostkaController _jednostkaCtrl;
        private readonly CenaController _cenaCtrl;

        public PozycjaZamowieniaModel pozycja;
        private ArtykulModel _wybranyArtykul;

        public Form_dodawaniePozycji()
        {
            this._jednostkaCtrl = new JednostkaController();
            this._cenaCtrl = new CenaController();

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_wybierzArtykul_Click(object sender, EventArgs e)
        {
            Form_artykuly artykuly = new Form_artykuly();
            if (artykuly.ShowDialog() == DialogResult.Yes)
            {
                this._wybranyArtykul = artykuly.wybranyArtykul;

                textBox_nazwa.Text = this._wybranyArtykul.NAZWA;
                textBox_indeksKatalogowy.Text = this._wybranyArtykul.INDEKS_KATALOGOWY;
                textBox_pole6.Text = this._wybranyArtykul.POLE6;

                ZamowienieModel zamowienie = ZamowienieController.PobierzZamowienie();

                CENA_ARTYKULU domyslnaCenaArtykulu = this._cenaCtrl.PobierzCeneArtykulu(this._wybranyArtykul.ID_CENY_DOM.Value, this._wybranyArtykul.ID_ARTYKULU);
                PobierzKursWalutyResult kursWaluty = this._cenaCtrl.PobierzKursWaluty(zamowienie.SYM_WAL, domyslnaCenaArtykulu.SYM_WAL, DateTime.Now);

                Decimal domyslnaCenaNettoWal = ((domyslnaCenaArtykulu.CENA_NETTO ?? 0) / (kursWaluty.KursSredni ?? 1));
                Decimal domyslnaCenaBruttoWal = ((domyslnaCenaArtykulu.CENA_BRUTTO ?? 0) / (kursWaluty.KursSredni ?? 1));

                numericUpDown_cenaNettoWal.Value = domyslnaCenaNettoWal;
                numericUpDown_cenaBruttoWal.Value = domyslnaCenaBruttoWal;
            }
        }

        private void button_wstawPozycje_Click(object sender, EventArgs e)
        {
            if ((textBox_nazwa.Text == string.Empty) || (textBox_indeksKatalogowy.Text == string.Empty))
            {
                MessageBox.Show("Wybierz artykuł.","Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return;
            }

            if ((numericUpDown_zamowiono.Value == 0) || (numericUpDown_cenaNettoWal.Value == 0) || (numericUpDown_cenaBruttoWal.Value == 0))
            {
                MessageBox.Show("Wprowadź wartości.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return;
            }

            ZamowienieModel zamowienie = ZamowienieController.PobierzZamowienie();
            TYP_DOKUMENTU_MAGAZYNOWEGO typZamowieniwa = ZamowienieController.PobierzTypZamowienia(zamowienie.NUMER, zamowienie.ID_FIRMY);
            JEDNOSTKA jednostkaArtykulu = this._jednostkaCtrl.PobierzJednostkePoId(this._wybranyArtykul.ID_JEDNOSTKI.Value); ;

            PobierzKursWalutyResult kursWaluty = this._cenaCtrl.PobierzKursWaluty("", zamowienie.SYM_WAL, DateTime.Now);

            Decimal domyslnaCenaNettoWal = numericUpDown_cenaNettoWal.Value;
            Decimal domyslnaCenaBruttoWal = numericUpDown_cenaBruttoWal.Value;

            Decimal domyslnaCenaNetto = (domyslnaCenaNettoWal / (kursWaluty.KursSredni ?? 1));
            Decimal domyslnaCenaBrutto = (domyslnaCenaBruttoWal / (kursWaluty.KursSredni ?? 1));

            decimal idDodanejPozycjiZamowienia = ZamowienieController.DodajPozyceZamowienia(
                idZamowienia: zamowienie.ID_ZAMOWIENIA,
                idArtykulu: this._wybranyArtykul.ID_ARTYKULU,
                kodVat: "23",
                zamowiono: numericUpDown_zamowiono.Value,
                zrealizowano: 0,
                zarezerwowano: 0,
                doRezerwacji: 0,
                cenaNetto: domyslnaCenaNetto,
                cenaBrutto: domyslnaCenaBrutto,
                cenaNettoWal: domyslnaCenaNettoWal,
                cenaBruttoWal: domyslnaCenaBruttoWal,
                przelicznik: (jednostkaArtykulu.PRZELICZNIK ?? 1),
                jednostka: jednostkaArtykulu.SKROT,
                narzut: 0,
                opis: "",
                znakNarzutu: 2,
                trybRejestracji:0 ,
                idDostawyRez: null,
                idWariantuProduktu: null,
                znacznikCeny: "k",
                nrSerii: ""
            );

            PobierzFormatNumeracjiResult pobierzFormatNumeracjiRes = ZamowienieController.PobierzFormatNumeracji(
                idFirmy: zamowienie.ID_FIRMY,
                dokument: 2,
                idTypu: zamowienie.TYP.Value,
                idZasobu: zamowienie.ID_MAGAZYNU
            );

            SumujZamowienieResult sumujZamowienieRes = ZamowienieController.SumujZamowienie(zamowienie.ID_ZAMOWIENIA);

            bool czyZamowienieZatwierdzony = ZamowienieController.ZatwierdzZamowienie(
                idZamowienia: zamowienie.ID_ZAMOWIENIA,
                idKontrahenta: zamowienie.ID_KONTRAHENTA.Value,
                idTypu: typZamowieniwa.ID_TYPU,
                numer: zamowienie.NUMER,
                numFormat: pobierzFormatNumeracjiRes.FormatNum,
                numOkresNumeracji: pobierzFormatNumeracjiRes.Okresnumeracji,
                numAuto: pobierzFormatNumeracjiRes.Parametr1,
                numNiezalezny: pobierzFormatNumeracjiRes.Parametr2,
                autonumer: zamowienie.AUTONUMER,
                idFirmy: zamowienie.ID_FIRMY,
                idMagazynu: zamowienie.ID_MAGAZYNU,
                data: zamowienie.DATA.Value,
                dataRealizacji: zamowienie.DATA_REALIZACJI.Value,
                zaliczka: zamowienie.ZALICZKA.Value,
                priorytet: zamowienie.PRIORYTET.Value,
                autoRezerwacja: zamowienie.AUTO_REZERWACJA.Value,
                nrZamKlienta: zamowienie.NR_ZAMOWIENIA_KLIENTA,
                typ: zamowienie.TYP.Value,
                idPracownika: zamowienie.ID_PRACOWNIKA.Value,
                przelicznikWal: zamowienie.PRZELICZNIK_WAL.Value,
                dataKursWal: zamowienie.DATA_KURS_WAL.Value,
                symWal: zamowienie.SYM_WAL,
                dokWal: zamowienie.DOK_WAL.Value,
                flagaStanu: zamowienie.FLAGA_STANU.Value,
                trybRejestracji: zamowienie.TRYBREJESTRACJI.Value,
                rabatNarzut: zamowienie.RABAT_NARZUT.Value,
                znakRabatu: ((zamowienie.RABAT_NARZUT > 0) ? (byte)2 : (byte)0),
                uwagi: zamowienie.UWAGI,
                informacjeDodatkowe: zamowienie.INFORMACJE_DODATKOWE,
                osobaZamawiajaca: zamowienie.OSOBA_ZAMAWIAJACA,
                idKontaktu: zamowienie.ID_KONTAKTU.Value,
                formaPlatnosci: zamowienie.FORMA_PLATNOSCI,
                dniPlatnosci: zamowienie.DNI_PLATNOSCI.Value,
                fakturaParagon: zamowienie.FAKTURA_DO_ZAMOWIENIA.Value,
                numerPrzesylki: zamowienie.NUMER_PRZESYLKI,
                idOperatoraPrzesylki: zamowienie.ID_OPERATORA_PRZESYLKI
            );

            pozycja = new PozycjaZamowieniaModel() {
                POLE6Artykulu = textBox_pole6.Text == string.Empty?"0": textBox_pole6.Text,
                CENA_BRUTTO_WAL = numericUpDown_cenaBruttoWal.Value,
                CENA_NETTO_WAL = numericUpDown_cenaNettoWal.Value,
                NAZWA = textBox_nazwa.Text,
                INDEKS_KATALOGOWY = textBox_indeksKatalogowy.Text,
                ZAMOWIONO = numericUpDown_zamowiono.Value
            };

            this.DialogResult = DialogResult.Yes;
        }
    }
}
