﻿namespace analizaPozycjiZamowienia
{
    partial class Form_edycjaPozycji
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.button_zmien = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_zamowiono = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_cenaNettoWal = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDown_cenaBruttoWal = new System.Windows.Forms.NumericUpDown();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_zamowiono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaNettoWal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaBruttoWal)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_zmien);
            this.panel1.Controls.Add(this.button_anuluj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 160);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 81);
            this.panel1.TabIndex = 0;
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(306, 26);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // button_zmien
            // 
            this.button_zmien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zmien.Location = new System.Drawing.Point(225, 26);
            this.button_zmien.Name = "button_zmien";
            this.button_zmien.Size = new System.Drawing.Size(75, 23);
            this.button_zmien.TabIndex = 1;
            this.button_zmien.Text = "Zmień";
            this.button_zmien.UseVisualStyleBackColor = true;
            this.button_zmien.Click += new System.EventHandler(this.button_zmien_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(100, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Zamówiono:";
            // 
            // numericUpDown_zamowiono
            // 
            this.numericUpDown_zamowiono.DecimalPlaces = 2;
            this.numericUpDown_zamowiono.Location = new System.Drawing.Point(171, 99);
            this.numericUpDown_zamowiono.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.numericUpDown_zamowiono.Name = "numericUpDown_zamowiono";
            this.numericUpDown_zamowiono.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_zamowiono.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(84, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Cena netto wal:";
            // 
            // numericUpDown_cenaNettoWal
            // 
            this.numericUpDown_cenaNettoWal.DecimalPlaces = 2;
            this.numericUpDown_cenaNettoWal.Location = new System.Drawing.Point(171, 73);
            this.numericUpDown_cenaNettoWal.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.numericUpDown_cenaNettoWal.Name = "numericUpDown_cenaNettoWal";
            this.numericUpDown_cenaNettoWal.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_cenaNettoWal.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Cena brutto wal:";
            // 
            // numericUpDown_cenaBruttoWal
            // 
            this.numericUpDown_cenaBruttoWal.DecimalPlaces = 2;
            this.numericUpDown_cenaBruttoWal.Location = new System.Drawing.Point(171, 47);
            this.numericUpDown_cenaBruttoWal.Maximum = new decimal(new int[] {
            90000000,
            0,
            0,
            0});
            this.numericUpDown_cenaBruttoWal.Name = "numericUpDown_cenaBruttoWal";
            this.numericUpDown_cenaBruttoWal.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown_cenaBruttoWal.TabIndex = 14;
            // 
            // Form_edycjaPozycji
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 241);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown_zamowiono);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDown_cenaNettoWal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDown_cenaBruttoWal);
            this.Controls.Add(this.panel1);
            this.Name = "Form_edycjaPozycji";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edycja pozycji";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_zamowiono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaNettoWal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_cenaBruttoWal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_zmien;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_zamowiono;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown_cenaNettoWal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown_cenaBruttoWal;
    }
}