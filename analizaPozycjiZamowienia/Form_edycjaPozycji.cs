﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Controllers;
using analizaPozycjiZamowienia.Model;

namespace analizaPozycjiZamowienia
{
    public partial class Form_edycjaPozycji : Form
    {
        private readonly CenaController _cenaCtrl;

        private PozycjaZamowieniaModel _pozycjaZamowienia;
        private List<PozycjaZamowieniaModel> _pozycjeZamowieniaOtymSamymIndeksieHandlowym;

        public Form_edycjaPozycji(PozycjaZamowieniaModel pozycja, List<PozycjaZamowieniaModel> pozycjeZamowieniaOtymSamymIndeksieHandlowym)
        {
            this._cenaCtrl = new CenaController();

            InitializeComponent();
            this._pozycjaZamowienia = pozycja;
            this._pozycjeZamowieniaOtymSamymIndeksieHandlowym = pozycjeZamowieniaOtymSamymIndeksieHandlowym;
            ZaladujDaneNaFormularz();
        }

        private void ZaladujDaneNaFormularz()
        {
            numericUpDown_cenaBruttoWal.Value = this._pozycjaZamowienia.CENA_BRUTTO_WAL??0;
            numericUpDown_cenaNettoWal.Value = this._pozycjaZamowienia.CENA_NETTO_WAL??0;
            numericUpDown_zamowiono.Value = this._pozycjaZamowienia.ZAMOWIONO;
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PoprawCeneOrazZamowionaIlosc(PozycjaZamowieniaModel pozycjaZamowienia)
        {
            pozycjaZamowienia.CENA_NETTO_WAL = numericUpDown_cenaNettoWal.Value;
            pozycjaZamowienia.CENA_BRUTTO_WAL = numericUpDown_cenaBruttoWal.Value;

            bool czyPozycjaZamowieniaPoprawiona = ZamowienieController.PoprawPozycjeZamowienia(pozycjaZamowienia, this._cenaCtrl);
        }

        private void button_zmien_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;

            this._pozycjaZamowienia.ZAMOWIONO = numericUpDown_zamowiono.Value;
            this.PoprawCeneOrazZamowionaIlosc(this._pozycjaZamowienia);

            foreach(PozycjaZamowieniaModel pozycjaZamowieniaOtymSamymIndeksieHandlowym in this._pozycjeZamowieniaOtymSamymIndeksieHandlowym)
            {
                this.PoprawCeneOrazZamowionaIlosc(pozycjaZamowieniaOtymSamymIndeksieHandlowym);
            }

            Close();
        }
    }
}
