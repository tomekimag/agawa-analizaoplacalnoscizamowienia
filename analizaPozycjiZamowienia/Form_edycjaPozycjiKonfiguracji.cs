﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Model;
using analizaPozycjiZamowienia.Controllers;

namespace analizaPozycjiZamowienia
{
    public partial class Form_edycjaPozycjiKonfiguracji : Form
    {
        private C_imag_analizy_configuracjaModel parametr;

        public Form_edycjaPozycjiKonfiguracji(C_imag_analizy_configuracjaModel parametr)
        {
            InitializeComponent();
            this.parametr = parametr;
            WyswietlDane();
        }

        private void WyswietlDane()
        {
            textBox_nazwaParametru.Text = parametr.pozycja_konfiguracji;
            numericUpDown_wartoscParametru.Value = parametr.wartosc_konfiguracji;
        }

        private void button_zapisz_Click(object sender, EventArgs e)
        {
            parametr.wartosc_konfiguracji = numericUpDown_wartoscParametru.Value;
            C_imag_analizy_configuracjaController.ModyfikujPozycjeKonfiguracji(parametr);
            this.DialogResult = DialogResult.Yes;
        }
    }
}
