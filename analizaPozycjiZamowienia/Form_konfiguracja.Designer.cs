﻿namespace analizaPozycjiZamowienia
{
    partial class Form_konfiguracja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView_konfiguracja = new System.Windows.Forms.DataGridView();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.button_edycja = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_konfiguracja)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_edycja);
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 286);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(643, 71);
            this.panel1.TabIndex = 0;
            // 
            // dataGridView_konfiguracja
            // 
            this.dataGridView_konfiguracja.AllowUserToAddRows = false;
            this.dataGridView_konfiguracja.AllowUserToDeleteRows = false;
            this.dataGridView_konfiguracja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_konfiguracja.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_konfiguracja.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_konfiguracja.Name = "dataGridView_konfiguracja";
            this.dataGridView_konfiguracja.ReadOnly = true;
            this.dataGridView_konfiguracja.Size = new System.Drawing.Size(643, 286);
            this.dataGridView_konfiguracja.TabIndex = 1;
            this.dataGridView_konfiguracja.DataSourceChanged += new System.EventHandler(this.dataGridView_konfiguracja_DataSourceChanged);
            this.dataGridView_konfiguracja.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_konfiguracja_ColumnAdded);
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(544, 22);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 0;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // button_edycja
            // 
            this.button_edycja.Location = new System.Drawing.Point(25, 22);
            this.button_edycja.Name = "button_edycja";
            this.button_edycja.Size = new System.Drawing.Size(75, 23);
            this.button_edycja.TabIndex = 1;
            this.button_edycja.Text = "Edycja";
            this.button_edycja.UseVisualStyleBackColor = true;
            this.button_edycja.Click += new System.EventHandler(this.button_edycja_Click);
            // 
            // Form_konfiguracja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 357);
            this.Controls.Add(this.dataGridView_konfiguracja);
            this.Controls.Add(this.panel1);
            this.Name = "Form_konfiguracja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Konfiguracja";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_konfiguracja)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView_konfiguracja;
        private System.Windows.Forms.Button button_edycja;
        private System.Windows.Forms.Button button_zamknij;
    }
}