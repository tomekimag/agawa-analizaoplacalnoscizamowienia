﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Model;
using analizaPozycjiZamowienia.Controllers;

namespace analizaPozycjiZamowienia
{
    public partial class Form_konfiguracja : Form
    {
        private List<C_imag_analizy_configuracjaModel> pozycjeKonfiguracji;

        public Form_konfiguracja()
        {
            InitializeComponent();
            ZaladujKonfiguracjeDoSiatki();
        }

        private void dataGridView_konfiguracja_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void ZaladujKonfiguracjeDoSiatki()
        {
            dataGridView_konfiguracja.DataSource = pozycjeKonfiguracji = C_imag_analizy_configuracjaController.PobierzPozycjeKonfiguracji();
        }

        private void dataGridView_konfiguracja_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "pozycja_konfiguracji":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Nazwa parametru";
                    break;
                case "wartosc_konfiguracji":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Wartość";
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_edycja_Click(object sender, EventArgs e)
        {
            if (dataGridView_konfiguracja.SelectedRows == null)
            {
                MessageBox.Show("Wskarz parametr.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_konfiguracja.SelectedRows.Count != 0)
            {
                int id = int.Parse(dataGridView_konfiguracja["id", dataGridView_konfiguracja.SelectedRows[0].Index].Value.ToString());

                C_imag_analizy_configuracjaModel parametr = pozycjeKonfiguracji.Where(x => x.id == id).FirstOrDefault();

                if (parametr != null)
                {
                    Form_edycjaPozycjiKonfiguracji edycjaKonfiguracji = new Form_edycjaPozycjiKonfiguracji(parametr);
                    if (edycjaKonfiguracji.ShowDialog() == DialogResult.Yes)
                    {
                        ZaladujKonfiguracjeDoSiatki();
                    }
                }
            }
        }
    }
}
