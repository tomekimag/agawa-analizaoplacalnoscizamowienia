﻿namespace analizaPozycjiZamowienia
{
    partial class Form_naglowki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox_edycja = new System.Windows.Forms.GroupBox();
            this.button_zapisz = new System.Windows.Forms.Button();
            this.textBox_etykieta = new System.Windows.Forms.TextBox();
            this.label_czy_edytowalny = new System.Windows.Forms.Label();
            this.checkbox_czy_edytowalny = new System.Windows.Forms.CheckBox();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.dataGridView_columny = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.groupBox_edycja.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_columny)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox_edycja);
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 342);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(631, 110);
            this.panel1.TabIndex = 0;
            // 
            // groupBox_edycja
            // 
            this.groupBox_edycja.Controls.Add(this.button_zapisz);
            this.groupBox_edycja.Controls.Add(this.textBox_etykieta);
            this.groupBox_edycja.Controls.Add(this.label_czy_edytowalny);
            this.groupBox_edycja.Controls.Add(this.checkbox_czy_edytowalny);
            this.groupBox_edycja.Location = new System.Drawing.Point(12, 12);
            this.groupBox_edycja.Name = "groupBox_edycja";
            this.groupBox_edycja.Size = new System.Drawing.Size(406, 79);
            this.groupBox_edycja.TabIndex = 3;
            this.groupBox_edycja.TabStop = false;
            this.groupBox_edycja.Text = "Edycja etykiety";
            // 
            // button_zapisz
            // 
            this.button_zapisz.Location = new System.Drawing.Point(303, 50);
            this.button_zapisz.Name = "button_zapisz";
            this.button_zapisz.Size = new System.Drawing.Size(97, 23);
            this.button_zapisz.TabIndex = 4;
            this.button_zapisz.Text = "Zapisz";
            this.button_zapisz.UseVisualStyleBackColor = true;
            this.button_zapisz.Click += new System.EventHandler(this.button_zapisz_Click);
            // 
            // textBox_etykieta
            // 
            this.textBox_etykieta.Location = new System.Drawing.Point(6, 19);
            this.textBox_etykieta.Name = "textBox_etykieta";
            this.textBox_etykieta.Size = new System.Drawing.Size(394, 20);
            this.textBox_etykieta.TabIndex = 3;
            // 
            // label_czy_edytowalny
            // 
            this.label_czy_edytowalny.Location = new System.Drawing.Point(39, 42);
            this.label_czy_edytowalny.Name = "label_czy_edytowalny";
            this.label_czy_edytowalny.Size = new System.Drawing.Size(82, 20);
            this.label_czy_edytowalny.TabIndex = 3;
            this.label_czy_edytowalny.Text = "Edytowalny";
            // 
            // checkbox_czy_edytowalny
            // 
            this.checkbox_czy_edytowalny.Location = new System.Drawing.Point(21, 39);
            this.checkbox_czy_edytowalny.Name = "checkbox_czy_edytowalny";
            this.checkbox_czy_edytowalny.Size = new System.Drawing.Size(21, 20);
            this.checkbox_czy_edytowalny.TabIndex = 3;
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(544, 75);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 0;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // dataGridView_columny
            // 
            this.dataGridView_columny.AllowUserToAddRows = false;
            this.dataGridView_columny.AllowUserToDeleteRows = false;
            this.dataGridView_columny.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_columny.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_columny.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_columny.Name = "dataGridView_columny";
            this.dataGridView_columny.ReadOnly = true;
            this.dataGridView_columny.Size = new System.Drawing.Size(631, 342);
            this.dataGridView_columny.TabIndex = 1;
            this.dataGridView_columny.DataSourceChanged += new System.EventHandler(this.dataGridView_columny_DataSourceChanged);
            this.dataGridView_columny.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_columny_ColumnAdded);
            this.dataGridView_columny.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_columny_RowEnter);
            // 
            // Form_naglowki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 452);
            this.Controls.Add(this.dataGridView_columny);
            this.Controls.Add(this.panel1);
            this.Name = "Form_naglowki";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nagłówki";
            this.panel1.ResumeLayout(false);
            this.groupBox_edycja.ResumeLayout(false);
            this.groupBox_edycja.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_columny)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.GroupBox groupBox_edycja;
        private System.Windows.Forms.Button button_zapisz;
        private System.Windows.Forms.TextBox textBox_etykieta;
        private System.Windows.Forms.Label label_czy_edytowalny;
        private System.Windows.Forms.CheckBox checkbox_czy_edytowalny;
        private System.Windows.Forms.DataGridView dataGridView_columny;
    }
}