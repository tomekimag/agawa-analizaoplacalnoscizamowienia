﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analizaPozycjiZamowienia.Model;
using analizaPozycjiZamowienia.Controllers;

namespace analizaPozycjiZamowienia
{
    public partial class Form_naglowki : Form
    {
        private List<imagTekstNaglowkowKolumnModel> tekstyNaglowkow;

        private int id = 0;

        public Form_naglowki()
        {
            InitializeComponent();
            ZaladujTekstyNaglowkowDoSiatki();
        }

        private void ZaladujTekstyNaglowkowDoSiatki()
        {
            dataGridView_columny.DataSource = tekstyNaglowkow = imagTekstNaglowkowKolumnController.PobierzTekstyNaglowkow();
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            Close();
        }

        private void button_zapisz_Click(object sender, EventArgs e)
        {
            if (textBox_etykieta.Text == string.Empty) { MessageBox.Show("Etykieta musi mieć długość większom niż 0 znaków.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
            imagTekstNaglowkowKolumnController.ZapiszTekstNaglowka(id, textBox_etykieta.Text, checkbox_czy_edytowalny.Checked);
            ZaladujTekstyNaglowkowDoSiatki();
        }

        private void dataGridView_columny_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                /*        public string nazwa_kolumny { get; set; }
        public string tekst_wyswietlany { get; set; }
*/
                case "nazwa_kolumny":
                    e.Column.Visible = true;
                    //e.Column.HeaderText = "marża 90%";
                    break;
                case "tekst_wyswietlany":
                    e.Column.Visible = true;
                    //e.Column.HeaderText = "marża 100%";
                    break;
                case "czy_edytowalny":
                    e.Column.Visible = true;
                    break;
                default:
                    e.Column.Visible = false;
                    break;
            }
        }

        private void dataGridView_columny_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridView_columny_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            id = int.Parse(dataGridView_columny["id", e.RowIndex].Value.ToString());
            textBox_etykieta.Text = dataGridView_columny["tekst_wyswietlany", e.RowIndex].Value.ToString();
            checkbox_czy_edytowalny.Checked = Convert.ToBoolean(dataGridView_columny["czy_edytowalny", e.RowIndex].Value);
        }
    }
}
