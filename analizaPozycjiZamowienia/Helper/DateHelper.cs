﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Helper
{
    public class DateHelper
    {
        private readonly static DateTime _minDateTime = new DateTime(1900, 1, 1);

        public static int DateToInt(string data)
        {
            DateTime d2 = _minDateTime;
            DateTime d = Convert.ToDateTime(data);

            if (d > _minDateTime)
            {
                double dif = (d.Date - d2).TotalDays;
                dif = dif + 36163;

                if (dif < 0)
                    dif = 0;

                return Convert.ToInt32(dif);
            }
            else
                return 0;
        }
        public static int TimeToInt(DateTime data)
        {
            int wynik = ((((data.Hour * 60) + data.Minute) * 60) + data.Second) * 100;
            return wynik;
        }
        public static int DateToInt(DateTime data)
        {
            if (data > _minDateTime)
            {
                DateTime d2 = new DateTime(1900, 1, 1);
                DateTime d = Convert.ToDateTime(data.Date);
                double dif = (d - d2).TotalDays;
                dif = dif + 36163;

                if (dif < 0)
                    dif = 0;

                return Convert.ToInt32(dif);
            }
            else
                return 0;
        }

        public static int DateToInt(DateTime? data)
        {
            if (data != null && data.Value > _minDateTime)
            {
                DateTime d2 = new DateTime(1900, 1, 1);
                DateTime d = Convert.ToDateTime(data.Value.Date);
                double dif = (d - d2).TotalDays;
                dif = dif + 36163;

                if (dif < 0)
                    dif = 0;

                return Convert.ToInt32(dif);
            }
            else
                return 0;
        }

        public static DateTime IntToDate(int data)
        {
            DateTime d2;
            int d = data - 36163;

            if (d > 0)
            {
                d2 = _minDateTime;
                d2 = d2.AddDays(d);
            }
            else
                d2 = DateTime.MinValue;

            return d2;
        }

        public static int TimeToInt(TimeSpan time)
        {
            int wynik = ((((time.Hours * 60) + time.Minutes) * 60) + time.Seconds) * 100;
            return wynik;
        }

        public static TimeSpan IntToTime(int timeInt)
        {
            int time_HMS = timeInt / 100;
            int time_HM = time_HMS % 60;
            int time_S = (time_HMS - time_HM * 60);
            int time_H = time_HM % 60;
            int time_M = (time_HM - time_H * 60);


            TimeSpan time = new TimeSpan(hours: time_H, minutes: time_M, seconds: time_S);
            return time;
        }

        public static DateTime IntToDateAndTime(int dateInt, int timeInt)
        {
            DateTime dateAndTime = IntToDate(dateInt) + IntToTime(timeInt);
            return dateAndTime;
        }
    }
}
