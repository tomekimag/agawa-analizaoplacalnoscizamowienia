﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Model
{
    public class ArtykulModel
    {
        public decimal ID_ARTYKULU { get; set; }
        public Nullable<decimal> ID_MAGAZYNU { get; set; }
        public Nullable<decimal> ID_CENY_DOM { get; set; }
        public string NAZWA_CENY_DOM { get; set; }
        public Nullable<decimal> ID_JEDNOSTKI { get; set; }
        public string JEDNOSTKA_SKROT { get; set; }
        public string NAZWA { get; set; }
        public string NAZWA_ORYG { get; set; }
        public Nullable<decimal> STAN { get; set; }
        public string INDEKS_KATALOGOWY { get; set; }
        public string INDEKS_HANDLOWY { get; set; }
        public string KOD_KRESKOWY { get; set; }
        public string POLE2 { get; set; }
        public string POLE3 { get; set; }
        public string POLE6 { get; set; }
    }
}
