﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Model
{
    public class C_imag_analizy_configuracjaModel
    {
        public int id { get; set; }
        public string pozycja_konfiguracji { get; set; }
        public decimal wartosc_konfiguracji { get; set; }
    }
}
