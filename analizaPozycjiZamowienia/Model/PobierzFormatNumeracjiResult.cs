﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Model
{
    public class PobierzFormatNumeracjiResult
    {
        public string FormatNum { get; set; }
        public byte Okresnumeracji { get; set; }
        public byte Parametr1 { get; set; }
        public byte Parametr2 { get; set; }
    }
}
