﻿using analizaPozycjiZamowienia.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Model
{
    public class PobierzKursWalutyResult
    {
        public Decimal? KursSredni { get; set; }
        public int? DataKursuInt { get; set; }
        public DateTime DataKursu
        {
            get { return DateHelper.IntToDate(this.DataKursuInt ?? 0); }
            set { this.DataKursuInt = DateHelper.DateToInt(value); }
        }
    }
}
