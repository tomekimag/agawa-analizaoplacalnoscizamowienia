﻿using analizaPozycjiZamowienia.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace analizaPozycjiZamowienia.Model
{
    public delegate void OnZmianaIlosciZamowionej(PozycjaZamowieniaModel pozycjaZamowienia);

    public class PozycjaZamowieniaModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event OnZmianaIlosciZamowionej ZmienionoIloscZamowiona;

        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Z tabeli artykul
        /// </summary>
        //public string INDEKS_KATALOGOWY { get; set; }
        private string _indeksHandlowy;
        public string INDEKS_HANDLOWY
        {
            get { return this._indeksHandlowy; }
            set
            {
                this._indeksHandlowy = value;
                this.NotifyPropertyChanged(nameof(this.INDEKS_HANDLOWY));
            }
        }

        /// <summary>
        /// Z tabeli artykul
        /// </summary>
        //public string INDEKS_KATALOGOWY { get; set; }
        private string _indeksKatalogowy;
        public string INDEKS_KATALOGOWY
        {
            get { return this._indeksKatalogowy; }
            set
            {
                this._indeksKatalogowy = value;
                this.NotifyPropertyChanged(nameof(this.INDEKS_KATALOGOWY));
            }
        }

        /// <summary>
        /// Z tabeli artykul
        /// </summary>
        //public string NAZWA { get; set; }
        private string _nazwa;
        public string NAZWA
        {
            get { return this._nazwa; }
            set
            {
                this._nazwa = value;
                this.NotifyPropertyChanged(nameof(this.NAZWA));
            }
        }

        public decimal ID_POZYCJI_ZAMOWIENIA { get; set; }
        public Nullable<decimal> ID_ZAMOWIENIA { get; set; }
        public Nullable<decimal> ID_ARTYKULU { get; set; }
        public Nullable<decimal> ID_WARIANTU { get; set; }
        public string KOD_VAT { get; set; }

        /// <summary>
        /// Z tabeli pozycja zamówienia
        /// </summary>
        //public decimal ZAMOWIONO { get; set; }
        private decimal _zamowiono;
        public decimal ZAMOWIONO
        {
            get { return this._zamowiono; }
            set
            {
                decimal zamowionoPrzed = this._zamowiono;
                decimal zamowionoPo = value;

                this._zamowiono = value;

                this.KOSZT_ZAKUPU_1_CTN = Math.Round((this.CENA_BRUTTO_WAL_PLUS_2_PROCENT ?? 0) * this.ZAMOWIONO * this.USD, 4);
                if(this.ZAMOWIONO != 0)
                    this.CIF_WARSZAWA = Math.Round(this.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN / this.ZAMOWIONO, 4);

                this.NotifyPropertyChanged(nameof(this.ZAMOWIONO));

                if (this.ZmienionoIloscZamowiona != null)
                    this.ZmienionoIloscZamowiona(this);
            }
        }

        public decimal ZREALIZOWANO { get; set; }
        public decimal DO_REALIZACJI { get; set; }
        public decimal ZAREZERWOWANO { get; set; }
        public decimal DO_REZERWACJI { get; set; }
        public decimal CENA_NETTO { get; set; }
        public decimal CENA_BRUTTO { get; set; }

        //Wyłączyć to pole w widoku
        //public Nullable<decimal> CENA_NETTO_WAL { get; set; }
        private Nullable<decimal> _cenaNettoWal;
        public Nullable<decimal> CENA_NETTO_WAL
        {
            get { return this._cenaNettoWal; }
            set
            {
                this._cenaNettoWal = value;
                this.NotifyPropertyChanged(nameof(this.CENA_NETTO_WAL));
            }
        }
        //public Nullable<decimal> CENA_BRUTTO_WAL_PLUS_2_PROCENT { get; set; }
        private Nullable<decimal> _cenaBruttoWalPlus2Procent;
        public Nullable<decimal> CENA_BRUTTO_WAL_PLUS_2_PROCENT
        {
            get { return this._cenaBruttoWalPlus2Procent; }
            set
            {
                this._cenaBruttoWalPlus2Procent = value;

                this.KOSZT_ZAKUPU_1_CTN = Math.Round((this.CENA_BRUTTO_WAL_PLUS_2_PROCENT ?? 0) * this.ZAMOWIONO * this.USD, 4);

                this.NotifyPropertyChanged(nameof(this.CENA_BRUTTO_WAL_PLUS_2_PROCENT));
            }
        }
        //public Nullable<decimal> CENA_BRUTTO_WAL { get; set; }
        private Nullable<decimal> _cenaBruttoWal;
        public Nullable<decimal> CENA_BRUTTO_WAL
        {
            get { return this._cenaBruttoWal; }
            set
            {
                this._cenaBruttoWal = value;

                this.CENA_BRUTTO_WAL_PLUS_2_PROCENT = Math.Round((this.CENA_BRUTTO_WAL ?? 0) * 102 / 100, 4);

                this.NotifyPropertyChanged(nameof(this.CENA_BRUTTO_WAL));
            }
        }

        public decimal PRZELICZNIK { get; set; }
        public string JEDNOSTKA { get; set; }
        public decimal NARZUT { get; set; }
        public decimal DO_REZ_USER { get; set; }
        public Nullable<decimal> DO_REZ_POP { get; set; }
        public decimal STAN_ZREALIZOWANO { get; set; }
        public Nullable<byte> TYP { get; set; }
        public string OPIS { get; set; }
        public Nullable<byte> TRYBREJESTRACJI { get; set; }
        public string ZNACZNIK_CENY { get; set; }
        public Nullable<decimal> ID_POZYCJI_OFERTY { get; set; }
        public Nullable<decimal> ID_DOSTAWY_REZ { get; set; }
        public string POLE1 { get; set; }
        public string POLE2 { get; set; }
        public string POLE3 { get; set; }
        public string POLE4 { get; set; }
        public string POLE5 { get; set; }
        public string POLE6 { get; set; }
        public string POLE7 { get; set; }
        public string POLE8 { get; set; }
        public string POLE9 { get; set; }
        public string POLE10 { get; set; }
        public string NR_SERII { get; set; }

        /// <summary>
        /// Z tabeli artykul 
        /// Pole6 jest to Objętość CTN
        /// </summary>
        //public string POLE6 { get; set; }
        private string _pole6Artykulu;
        public string POLE6Artykulu
        {
            get { return this._pole6Artykulu; }
            set
            {
                this._pole6Artykulu = value;

                this.CBM = ((this.POLE6Artykulu ?? "") == "" ? (decimal)0 : decimal.Parse(this.POLE6Artykulu.Trim().Replace('.', ',')));

                this.NotifyPropertyChanged(nameof(this.POLE6Artykulu));
            }
        }

        /// <summary>
        /// Pole w konfiguracji
        /// </summary>
        //public decimal KOSZT_TRANS_PLN { get; set; }
        private decimal _kosztyTransPln;
        public decimal KOSZT_TRANS_PLN
        {
            get { return this._kosztyTransPln; }
            set
            {
                this._kosztyTransPln = value;

                if(this.HQ40 != 0)
                    this.JEDNA_SETNA_METRA_SZESCIENNEGO = Math.Round(this.KOSZT_TRANS_PLN / this.HQ40 / 100, 4);

                this.NotifyPropertyChanged(nameof(this.KOSZT_TRANS_PLN));
            }
        }

        /// <summary>
        /// KOSZT_TRANS_PLN/hq40/100
        /// </summary>
        //public decimal JEDNA_SETNA_METRA_SZESCIENNEGO { get; set; }
        private decimal _jednaSetnaMetraSzesciennego;
        public decimal JEDNA_SETNA_METRA_SZESCIENNEGO
        {
            get { return this._jednaSetnaMetraSzesciennego; }
            set
            {
                this._jednaSetnaMetraSzesciennego = value;
                this.NotifyPropertyChanged(nameof(this.JEDNA_SETNA_METRA_SZESCIENNEGO));
            }
        }

        /// <summary>
        /// Wpisywane na formularzu.
        /// </summary>
        //public decimal USD { get; set; }
        private decimal _usd;
        public decimal USD
        {
            get { return this._usd; }
            set
            {
                this._usd = value;

                this.KOSZT_ZAKUPU_1_CTN = Math.Round((this.CENA_BRUTTO_WAL_PLUS_2_PROCENT ?? 0) * this.ZAMOWIONO * this.USD, 4);

                this.MARZA_80_PROCENT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)1.8 * this.USD, 4);
                this.MARZA_90_PROCENT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)1.9 * this.USD, 4);
                this.MARZA_100_PROCENT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)2.0 * this.USD, 4);

                this.NotifyPropertyChanged(nameof(this.USD));
            }
        }

        /// <summary>
        /// Pole w konfiguracji
        /// </summary>
        //public decimal CENA_TRANSPORTU_PLN { get; set; }
        private decimal _cenaTransportuPln;
        public decimal CENA_TRANSPORTU_PLN
        {
            get { return this._cenaTransportuPln; }
            set
            {
                this._cenaTransportuPln = value;

                if(this.HQ40 != 0)
                    this.KOSZT_JEDNEGO_METRA_SZESCIENNEGO = Math.Round(this.CENA_TRANSPORTU_PLN / this.HQ40, 4);

                this.NotifyPropertyChanged(nameof(this.CENA_TRANSPORTU_PLN));
            }
        }

        /// <summary>
        /// Pole w konfiguracji
        /// </summary>
        //public decimal HQ40 { get; set; }
        private decimal _hq40;
        public decimal HQ40
        {
            get { return this._hq40; }
            set
            {
                this._hq40 = value;

                if (this.HQ40 != 0)
                {
                    this.JEDNA_SETNA_METRA_SZESCIENNEGO = Math.Round(this.KOSZT_TRANS_PLN / this.HQ40 / 100, 4);
                    this.KOSZT_JEDNEGO_METRA_SZESCIENNEGO = Math.Round(this.CENA_TRANSPORTU_PLN / this.HQ40, 4);
                }

                this.NotifyPropertyChanged(nameof(this.HQ40));
            }
        }

        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal KOSZT_JEDNEGO_METRA_SZESCIENNEGO { get; set; }
        private decimal _kosztJednegoMetraSzczesciennego;
        public decimal KOSZT_JEDNEGO_METRA_SZESCIENNEGO
        {
            get { return this._kosztJednegoMetraSzczesciennego; }
            set
            {
                this._kosztJednegoMetraSzczesciennego = value;

                this.KOSZT_TRANSPORTU_1_CTN = Math.Round(this.CBM * this.KOSZT_JEDNEGO_METRA_SZESCIENNEGO, 4);

                this.NotifyPropertyChanged(nameof(this.KOSZT_JEDNEGO_METRA_SZESCIENNEGO));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal KOSZT_TRANSPORTU_1_CTN { get; set; }
        private decimal _kosztTransportu1ctn;
        public decimal KOSZT_TRANSPORTU_1_CTN
        {
            get { return this._kosztTransportu1ctn; }
            set
            {
                this._kosztTransportu1ctn = value;

                this.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN = Math.Round(this.KOSZT_TRANSPORTU_1_CTN + this.KOSZT_ZAKUPU_1_CTN, 4);

                this.NotifyPropertyChanged(nameof(this.KOSZT_TRANSPORTU_1_CTN));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal KOSZT_ZAKUPU_1_CTN { get; set; }
        private decimal _kosztZakupu1ctn;
        public decimal KOSZT_ZAKUPU_1_CTN
        {
            get { return this._kosztZakupu1ctn; }
            set
            {
                this._kosztZakupu1ctn = value;

                this.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN = Math.Round(this.KOSZT_TRANSPORTU_1_CTN + this.KOSZT_ZAKUPU_1_CTN, 4);

                this.NotifyPropertyChanged(nameof(this.KOSZT_ZAKUPU_1_CTN));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN { get; set; }
        private decimal _kosztTransportu1ctnIkosztZakupu1ctn;
        public decimal KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN
        {
            get { return this._kosztTransportu1ctnIkosztZakupu1ctn; }
            set
            {
                this._kosztTransportu1ctnIkosztZakupu1ctn = value;

                if(this.ZAMOWIONO != 0)
                    this.CIF_WARSZAWA = Math.Round(this.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN / this.ZAMOWIONO, 4);

                this.NotifyPropertyChanged(nameof(this.KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal CIF_WARSZAWA { get; set; }
        private decimal _cifWarszawa;
        public decimal CIF_WARSZAWA
        {
            get { return this._cifWarszawa; }
            set
            {
                this._cifWarszawa = value;

                this.ZERO = Math.Round(this.CIF_WARSZAWA * (decimal)1.047 * (decimal)1.23, 4);
                this.PIECDZIESIAT = Math.Round(this.CIF_WARSZAWA * (decimal)1.047 * (decimal)1.23 * (decimal)1.5, 4);
                this.SZESCDZIESIAT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.23 * (decimal)1.6, 4);
                this.SIEDEMDZIESIAT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.23 * (decimal)1.7, 4);
                this.MARZA_80_PROCENT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)1.8 * this.USD, 4);
                this.MARZA_90_PROCENT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)1.9 * this.USD, 4);
                this.MARZA_100_PROCENT = Math.Round(this.CIF_WARSZAWA * (decimal)1.046 * (decimal)1.22 * (decimal)2.0 * this.USD, 4);

                this.NotifyPropertyChanged(nameof(this.CIF_WARSZAWA));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal ZERO { get; set; }
        private decimal _zero;
        public decimal ZERO
        {
            get { return this._zero; }
            set
            {
                this._zero = value;
                this.NotifyPropertyChanged(nameof(this.ZERO));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal PIECDZIESIAT { get; set; }
        private decimal _piecdziesiat;
        public decimal PIECDZIESIAT
        {
            get { return this._piecdziesiat; }
            set
            {
                this._piecdziesiat = value;
                this.NotifyPropertyChanged(nameof(this.PIECDZIESIAT));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal SZESCDZIESIAT { get; set; }
        private decimal _szescdziesiat;
        public decimal SZESCDZIESIAT
        {
            get { return this._szescdziesiat; }
            set
            {
                this._szescdziesiat = value;
                this.NotifyPropertyChanged(nameof(this.SZESCDZIESIAT));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal SIEDEMDZIESIAT { get; set; }
        private decimal _siedemdziesiat;
        public decimal SIEDEMDZIESIAT
        {
            get { return this._siedemdziesiat; }
            set
            {
                this._siedemdziesiat = value;
                this.NotifyPropertyChanged(nameof(this.SIEDEMDZIESIAT));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal MARZA_80_PROCENT { get; set; }
        private decimal _marza80procent;
        public decimal MARZA_80_PROCENT
        {
            get { return this._marza80procent; }
            set
            {
                this._marza80procent = value;
                this.NotifyPropertyChanged(nameof(this.MARZA_80_PROCENT));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal MARZA_90_PROCENT { get; set; }
        private decimal _marza90procent;
        public decimal MARZA_90_PROCENT
        {
            get { return this._marza90procent; }
            set
            {
                this._marza90procent = value;
                this.NotifyPropertyChanged(nameof(this.MARZA_90_PROCENT));
            }
        }
        /// <summary>
        /// Pola formuł
        /// </summary>
        //public decimal MARZA_100_PROCENT { get; set; }
        private decimal _marza100procent;
        public decimal MARZA_100_PROCENT
        {
            get { return this._marza100procent; }
            set
            {
                this._marza100procent = value;
                this.NotifyPropertyChanged(nameof(this.MARZA_100_PROCENT));
            }
        }

        private decimal _cbm;
        public decimal CBM
        {
            get { return this._cbm; }
            set
            {
                this._cbm = value;

                this.KOSZT_TRANSPORTU_1_CTN = Math.Round(this.CBM * this.KOSZT_JEDNEGO_METRA_SZESCIENNEGO, 4);

                this.NotifyPropertyChanged(nameof(this.CBM));
            }
        }
    }
}
