﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Model
{
    public class SumujZamowienieResult
    {
        public decimal SumaNetto { get; set; }
        public decimal SumaBrutto { get; set; }
        public decimal SumaNettoWal { get; set; }
        public decimal SumaBruttoWal { get; set; }
    }
}
