﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analizaPozycjiZamowienia.Model
{
    public class imagTekstNaglowkowKolumnModel
    {
        public int id { get; set; }
        public string nazwa_kolumny { get; set; }
        public string tekst_wyswietlany { get; set; }
        public bool czy_edytowalny { get; set; }
    }
}
