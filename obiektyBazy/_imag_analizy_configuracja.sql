create table _imag_analizy_configuracja
(
	id int not null identity(1, 1) primary key,
	pozycja_konfiguracji varchar(200) not null,
	wartosc_konfiguracji decimal(14, 4) not null
)
go
insert into _imag_analizy_configuracja (pozycja_konfiguracji, wartosc_konfiguracji) values('TRANS', 16000.0000)
insert into _imag_analizy_configuracja (pozycja_konfiguracji, wartosc_konfiguracji) values('USD', 0.6200)
insert into _imag_analizy_configuracja (pozycja_konfiguracji, wartosc_konfiguracji) values('cena trans w PLN', 16000.0000)
insert into _imag_analizy_configuracja (pozycja_konfiguracji, wartosc_konfiguracji) values('hq40', 68.0000)
go
