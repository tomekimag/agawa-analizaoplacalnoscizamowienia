IF(not EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = '_imag_tekst_naglowkow_kolumn'))
BEGIN
    EXEC('create table _imag_tekst_naglowkow_kolumn
		(
			id int not null identity(1, 1) primary key,
			nazwa_kolumny varchar(100) not null,
			tekst_wyswietlany varchar(100) not null
		)')

	EXEC('
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''INDEKS_KATALOGOWY'', ''SYMBOL'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''NAZWA'', ''NAZWA'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''ZAMOWIONO'', ''PAK'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''CENA_BRUTTO_WAL_PLUS_2_PROCENT'', ''CENA + 2%'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''CENA_BRUTTO_WAL'', ''Cena rmb'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''POLE6'', ''CBM'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''CENA_BRUTTO_WAL'', ''Cena rmb'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''KOSZT_TRANS_PLN'', ''TRANS'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''JEDNA_SETNA_METRA_SZESCIENNEGO'', ''0,01m3'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''USD'', ''USD'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''CENA_TRANSPORTU_PLN'', ''cena trans w PLN'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''HQ40'', ''hq40'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''KOSZT_JEDNEGO_METRA_SZESCIENNEGO'', ''koszt 1m3 PLN'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''KOSZT_TRANSPORTU_1_CTN'', ''koszt trans 1ctn'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''KOSZT_ZAKUPU_1_CTN'', ''koszt zakupu ctn'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''KOSZT_TRANSPORTU_1_CTN_I_KOSZT_ZAKUPU_1_CTN'', ''koszt trans Ctn + koszt zak. Ctn'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''CIF_WARSZAWA'', ''cif warszawa'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''ZERO'', ''0'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''PIECDZIESIAT'', ''50'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''SZESCDZIESIAT'', ''60'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''SIEDEMDZIESIAT'', ''70'')
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''MARZA_80_PROCENT'', ''mar�a80%'')         
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''MARZA_90_PROCENT'', ''mar�a 90%'')            
		insert into _imag_tekst_naglowkow_kolumn (nazwa_kolumny, tekst_wyswietlany) values (''MARZA_100_PROCENT'', ''mar�a 100%'')  
	')
END

IF NOT EXISTS (SELECT * FROM   sys.columns WHERE  object_id = OBJECT_ID(N'_imag_tekst_naglowkow_kolumn') AND name = 'czy_edytowalny')
BEGIN
    ALTER TABLE _imag_tekst_naglowkow_kolumn ADD [czy_edytowalny] BIT NOT NULL DEFAULT(0)
END